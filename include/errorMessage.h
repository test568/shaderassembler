#ifndef ERROR_MESSAGE_H
#define ERROR_MESSAGE_H

#include <string>

enum TypeError
{
    CRITICAL = 0,
    WARNING = 1
};

struct Error
{
    TypeError type;
    std::string message;
};

#endif