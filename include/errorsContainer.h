#ifndef ERRORS_CONTAINER_H
#define ERRORS_CONTAINER_H

#include <list>

#include "errorMessage.h"

class ErrorsContainer
{
private:
    std::list<Error> errors;
    int wasCriticalErros;

public:
    ErrorsContainer();

    void push_back(Error _error);

    bool hasFatalErrors();

    const std::list<Error> *getListErrors() const { return &errors; }
};

#endif