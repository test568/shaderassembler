#ifndef PARSER_PROPERTIES_H
#define PARSER_PROPERTIES_H

#include <string>
#include <vector>
#include <list>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "errorMessage.h"
#include "errorsContainer.h"

#include "shader/property.h"



class ParserProperties
{
private:
    lua_State *L;

    ErrorsContainer *errors;

    int indexProperties;

    bool wasParse;

    std::list<Property *> properties;

public:
    ParserProperties(lua_State *L, ErrorsContainer *_errors, int _indexProperties);

    std::list<Property *> generateListProperties();

private:
    bool checkBeingProperty(std::string name, std::list<Property *> *properties);

    Property *generatePropery(std::string name, std::string type);

    Property *generateIntPropery();

    Property *generateFloatPropery();

    Property *generateTogglePropery();

    Property *generateVectorPropery();

    Property *generateColorPropery();

    Property *generateTexturePropery(bool _isHDRTypeTexture);

    Property *generateCubeMapPropery();

};

#endif