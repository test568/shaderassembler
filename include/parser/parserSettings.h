#ifndef PARSER_SETTINGS_H
#define PARSER_SETTINGS_H

#include <string>
#include <vector>
#include <list>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "errorMessage.h"
#include "errorsContainer.h"

#include "shader/settings.h"

#define NAME_CHANEL_R "R_Chanel"
#define NAME_CHANEL_G "G_Chanel"
#define NAME_CHANEL_B "B_Chanel"
#define NAME_CHANEL_A "A_Chanel"


class ParserSettings
{
private:
    lua_State *L;
    ErrorsContainer *errors;
    int posSettings;

    Settings *settings;

public:
    static void addMacroses(lua_State *L);

    ParserSettings(lua_State *L, ErrorsContainer *_errors, int _posSettings);

    void generateSettings();

    Settings *getSettings();

private:
    void parseNormalMap();

    void parseClippingSettings();
};

#endif