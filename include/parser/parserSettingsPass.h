#ifndef PARSER_SETTINGS_PASS_H
#define PARSER_SETTINGS_PASS_H

#include <string>
#include <vector>
#include <list>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "errorMessage.h"
#include "errorsContainer.h"

#include "shader/settingsPass.h"

class ParserSettingsPass
{
private:
    lua_State *L;
    ErrorsContainer *errors;

public:
    ParserSettingsPass(lua_State *L, ErrorsContainer *_errors);

    SettingsPass generateSettingsPass();

    SettingsPass updateSettingsPass(SettingsPass settings);
    
};

#endif