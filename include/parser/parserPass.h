#ifndef PARSER_PASS_H
#define PARSER_PASS_H

#include <string>
#include <vector>
#include <list>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "errorMessage.h"
#include "errorsContainer.h"

#include "shader/pass.h"

class ParserPass
{
private:
    lua_State *L;
    ErrorsContainer *errors;
    
    bool parseWithErros;

    Pass *pass;

public:
    ParserPass(lua_State *L, ErrorsContainer *_errors);

    void generatePass(SettingsPass _globalSettings);

    Pass *getPass();

private:
    void parseNamePass();

    void parseTypePass();
 
    void parsePriorityPass();

    void parseAdditionalDataForSurface();

    void parseIsUseDefaultPrograms();

    void parseListLibgs();

    void parseMainShaders();

    void parseDefines();

    void parseGroupStrings(const char *_nameGroup, std::vector<std::string> *_dest);

    std::string readAllFile(const std::string *_path);
};

#endif