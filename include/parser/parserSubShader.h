#ifndef PARSER_SUB_SHADER_H
#define PARSER_SUB_SHADER_H

#include <string>
#include <vector>
#include <list>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "errorMessage.h"
#include "errorsContainer.h"

#include "parserPass.h"
#include "parserSettingsPass.h"

class ParserSubShader
{
private:
    lua_State *L;
    ErrorsContainer *errors;
    int indexSubShader;

    std::vector<Pass *> passes;

public:
    ParserSubShader(lua_State *L, ErrorsContainer *_errors, int _indexSubShader);

    void generateSubShader();

    const std::vector<Pass *> *getPasses() const { return &passes; }
};

#endif