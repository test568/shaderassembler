#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <vector>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "errorMessage.h"
#include "errorsContainer.h"

#include "parserProperties.h"
#include "parserSettings.h"
#include "parserSubShader.h"

class Parser
{
private:
    lua_State *L;

    ErrorsContainer *errors;

private:
    ParserSettings *parserSettings;
    ParserProperties *parserProperties;
    ParserSubShader *parserSubShader;

    std::string nameShader;

public:
    Parser(ErrorsContainer *_errors);

    void parse(std::string path);

    std::string getNameShader();

    std::list<Property *> getProperties();

    const std::vector<Pass *> *getPasses();

    Settings *getSettings();
};

#endif