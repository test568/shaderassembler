#ifndef SHADER_WRITER_H
#define SHADER_WRITER_H

#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include <list>

#include "errorsContainer.h"

#include <shader/shader.h>
#include <shader/settingsPass.h>
#include <shader/program.h>
#include <shader/pass.h>
#include <shader/property.h>

class ShaderWriter
{
private:
    ErrorsContainer *errors;

public:
    ShaderWriter(ErrorsContainer *_errors);

    void writeShader(const std::string *_path, const Shader *_shader);

private:
    void writeProperties(SDL_RWops *_file, const Shader *_shader);

    void writeSettings(SDL_RWops *_file, const Shader *_shader);

    void writeProperty(SDL_RWops *_file, const Property *_property);

    void writePass(SDL_RWops *_file, const Pass *_pass);

    void writeSettingPass(SDL_RWops *_file, const SettingsPass *_settingPass);

    void writeProgram(SDL_RWops *_file, const Program *_program);

    void writeInt(SDL_RWops *_file, int _val);

    void writeChar(SDL_RWops *_file, char _val);

    void writeFloat(SDL_RWops *_file, float _val);

    void writeString(SDL_RWops *_file, const std::string *_val);

    void writeBool(SDL_RWops *_file, bool _val);
};

#endif