#ifndef ADDITIONAL_PROPERTIES_H
#define ADDITIONAL_PROPERTIES_H

#include <string>
#include <vector>

class AdditionalProperties
{
public:
    static std::vector<std::string> genericProperties;

    static std::vector<std::string> depthPassProperties;

    static std::vector<std::string> depthNormalPassProperties;

    static std::vector<std::string> normalPassProperties;

    static std::vector<std::string> baseColorPassProperties;

    static std::vector<std::string> surfacePassProperties;

    static std::vector<std::string> shadowPassProperties;
};
#endif