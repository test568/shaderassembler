#ifndef COMPILER_H
#define COMPILER_H

#include <vector>
#include <string>
#include <list>

#include "errorMessage.h"
#include "errorsContainer.h"

#include "shader/shader.h"

#include "loaderOpenGLContext.h"
#include "checkerProgram.h"
#include "additionalProperties.h"
#include "addtitionalDefines.h"

#include "programGenerator.h"
#include "checkerProgram.h"

class Compiler
{
private:
    ErrorsContainer *errors;

    LoaderOpenGLContext loaderOpenGlContext;
    ProgramGenerator programGenerator;

public:
    Compiler(ErrorsContainer *_errors);

    bool compileShader(Shader *_shader);

private:
    void compileSettings(Shader *_shader);

    void compilePass(Shader *_shader, Pass *_pass);

    int getCountExcessDefines(std::vector<std::string> *_genericDefines, std::vector<std::string> *_specialDefines);

    std::vector<std::string> compileDefines(std::vector<std::string> *_genericDefines, std::vector<std::string> *_specialDefines);

    int generateMaskDefines(const std::vector<std::string> *_defines, const std::list<Property *> *_properties);

    void removeNonUseDefines(Pass *_pass);

    std::vector<std::string> getAdditionalProperties(Pass *_pass);

    void addAdditionalDefines(Pass *_pass);
};

#endif