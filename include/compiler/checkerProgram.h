#ifndef CHECKER_PROGRAM_H
#define CHECKER_PROGRAM_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <string>
#include <vector>
#include <list>

#include "errorsContainer.h"
#include "shader/pass.h"
#include "shader/property.h"

class CheckerProgram
{
private:
    GLuint program;

    bool wasErrors;

    ErrorsContainer *errors;

public:
    CheckerProgram(const std::string *_program, GLenum _type, ErrorsContainer *_errors);

    ~CheckerProgram();

    bool compileWithErrors();

    std::vector<std::string> getUseUniformBlocks(std::vector<std::string> *_uniformBlocksNames);

    std::list<std::pair<std::string, TypeProperty>> getUseProperties(std::list<Property *> *_properties, std::vector<std::string> *_nonDefineProperties);


};

#endif