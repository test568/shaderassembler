#ifndef LOADER_OPEN_GL_CONTEXT_H
#define LOADER_OPEN_GL_CONTEXT_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <SDL2/SDL.h>

class LoaderOpenGLContext
{
private:
    SDL_Window *window;
    SDL_GLContext context;

    bool errorInit;

public:
    LoaderOpenGLContext();

    ~LoaderOpenGLContext();

    void load();

    void unload();

    bool allNormal();

};

#endif