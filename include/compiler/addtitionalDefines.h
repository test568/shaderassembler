#ifndef ADDITIONAL_DEFINES_H
#define ADDITIONAL_DEFINES_H

#include <string>
#include <vector>

class AdditionalDefines
{
public:
    static std::vector<std::string> genericDefines;

    static std::vector<std::string> vertexDefines;

    static std::vector<std::string> fragmentDefines;

    static std::vector<std::string> baseColorPassDefines;

    static std::vector<std::string> surfacePassDefines;

    static std::vector<std::string> depthPassDefines;

    static std::vector<std::string> normalPassDefines;

    static std::vector<std::string> depthNormalPassDefines;

    static std::vector<std::string> shadowPassDefines;
};

#endif