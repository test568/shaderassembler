#ifndef SPECIAL_UNIFORM_BLOCKS_H
#define SPECIAL_UNIFORM_BLOCKS_H

#include <string>
#include <vector>

class SpecialUniformBlocks
{
public:
    static std::vector<std::string> specialUniformBlocks;
};
#endif