#ifndef PROGRAM_GENERATOR_H
#define PROGRAM_GENERATOR_H

#include <string>
#include <vector>

#include "shader/pass.h"

#define VERSION_PROGRAM "#version 460 core\n"

#define IF_USE_INSTANCING_DDEFINE "#define USE_INSTANCING\n"
#define MAX_COUNT_INSTANCED_OBJECTS "#define MAX_COUNT_INSTANC_OBJECTS 500\n"

#define IF_USE_ADDITIONAL_LAYER "USE_ADDITIONAL_LAYER_"

class ProgramGenerator
{
public:
    void generateMainPrograms(Pass *_pass);

    std::string generateVertexProgram(const Pass *_pass, const std::vector<std::string> *_defines);

    std::string generateFragmentProgram(const Pass *_pass, const std::vector<std::string> *_defines);    

    std::vector<std::string> getAdditionalsPassDefines(const Pass *_pass);

    std::vector<std::vector<std::string>> generateCombinateDefinesVertex(const Pass *_pass);

    std::vector<std::vector<std::string>> generateCombinateDefinesFragment(const Pass *_pass); 

    std::string generateCodeDefines(const std::vector<std::string> *_defines);

private:
    std::vector<std::vector<std::string>> generateCombitations(const std::vector<std::string> *_combinations);

};

#endif