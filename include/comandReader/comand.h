#ifndef COMAND_H
#define COMAND_H

#include <string>

struct Comand
{
    std::string path;
    std::string compilePath;
};

#endif