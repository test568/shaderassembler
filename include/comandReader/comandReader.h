#ifndef COMAND_READER_H
#define COMAND_READER_H

#include "comand.h"

class ComandReader
{
public:
    static Comand *readComand(int argc, char *argv[]);

private:
    static bool checkIsInput(char *arg);

    static bool checkIsOutput(char *arg);

    static std::string getPath(char *arg);
};

#endif