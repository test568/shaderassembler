#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>

enum TypeClippingChanel
{
    Type_Clipping_Chanel_R = 0,
    Type_Clipping_Chanel_G = 1,
    Type_Clipping_Chanel_B = 2,
    Type_Clipping_Chanel_A = 3,
};

struct Settings
{
    std::string normalMap;
    std::string clippingTexture;
    //-----
    TypeClippingChanel typeClippingChanel;
    float clippingValue;
    bool inverseClipping;

    int indexNormalMapProperty;
    int indexclippingTextureProperty;
};

#endif