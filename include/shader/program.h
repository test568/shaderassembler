#ifndef PROGRAM_H
#define PROGRAM_H

#include <list>
#include <vector>
#include <string>

#include "shader/typeProperty.h"

struct Program
{
    int maskDefines;
    std::string program;

    std::list<std::pair<std::string, TypeProperty>> propertiesUsage;

    std::vector<std::string> uniformBlocksUsage;
};

#endif