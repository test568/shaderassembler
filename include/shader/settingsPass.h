#ifndef SETTINGS_PASS_H
#define SETTINGS_PASS_H

enum QueueRender : int
{
    QueueRender_Geometry = 1,
    QueueRender_AlphaTest = 2,
    QueueRender_Transparent = 4,
    QueueRender_Overlay = 8,
};

enum RenderType : int
{
    RenderType_Opaque = 1,
    RenderType_TransparentCutout = 2,
    RenderType_Transparent = 4,
    RenderType_Terrain = 8,
    RenderType_TerrainTree = 16,
    RenderType_Water = 32,
};

enum CullType : int
{
    CullType_Back = 0,
    CullType_Front = 1,
    CullType_Off = 2,
};

struct SettingsPass
{
    bool zWrite;
    bool zTest;

    RenderType renderType;
    QueueRender queueRender;
    CullType cullType;
    bool ganerateInstancing;
};

#endif