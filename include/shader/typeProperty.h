#ifndef TYPE_PROPERTY_H
#define TYPE_PROPERTY_H

enum TypeProperty : char
{
    Internal = -1,
    Texture = 0,
    Int = 1,
    Float = 2,
    Toggle = 3,
    CubeMap = 4,
    Vector = 5,
    Color = 6,
};

#endif