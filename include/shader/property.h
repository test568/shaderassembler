#ifndef PROPERTY_H
#define PROPERTY_H

#include <string>

#include "typeProperty.h"

struct Property
{
    std::string name;
    TypeProperty type;
};

#endif