#ifndef PASS_H
#define PASS_H

#include <list>
#include <string>

#include "settingsPass.h"
#include "program.h"

enum TypePass : int
{
    Depth = 1,
    Normal = 2,
    DepthNormal = 4,
    BaseColor = 8,
    Shadow = 16,
    Surface = 32,
};

#define MAX_COUNT_ADDITIONAL_LAYERS_IN_G_BUFFER 2

struct Pass
{
    std::string name;
    SettingsPass settingPass;
    TypePass typePass;
    int priority;

    char *additionalData;
    int sizeAdditionalData;

    bool useDefaultPrograms;
    bool useNormalMap;
    bool useClippingMap;

    std::vector<std::string> staticDefines;
    std::vector<std::string> genericDefines;
    std::vector<std::string> vertexDefines;
    std::vector<std::string> fragmentDefines;

    int maskUsingDefinesVertex;
    int maskUsingDefinesFragment;

    std::vector<std::string> libs;

    std::string fragment;
    std::string vertex;

    // init on compile stage
    std::string fragmentFull;
    std::string vertexFull;

    std::list<Program> vertexPrograms;
    std::list<Program> fragmentPrograms;
};

#endif