#ifndef SHADER_H
#define SHADER_H

#include <list>
#include <string>

#include "property.h"
#include "settings.h"
#include "pass.h"

class Shader
{
public:
    std::string name;

    std::list<Property *> properties;
    Settings *settings;
    std::vector<Pass *> passes;
};

#endif