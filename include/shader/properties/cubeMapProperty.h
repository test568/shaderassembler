#ifndef CUBE_MAP_PROPERTY_H
#define CUBE_MAP_PROPERTY_H

#include "property.h"

#define TYPE_CUBE_MAP_PROPERTY "CubeMap"

struct CubeMapProperty : public Property
{
    std::string defaultPath;
};

#endif