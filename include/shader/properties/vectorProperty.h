#ifndef VECTOR_PROPERTY_H
#define VECTOR_PROPERTY_H

#include "property.h"

#define TYPE_VECTOR_PROPERTY "Vector"

struct VectorProperty : public Property
{
    float defaultX;
    float defaultY;
    float defaultZ;
    float defaultW;
};

#endif