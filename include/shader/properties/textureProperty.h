#ifndef TEXTURE_PROPERTY_H
#define TEXTURE_PROPERTY_H

#include "property.h"

#define TYPE_TEXTURE_PROPERTY "Texture"
#define TYPE_HDR_TEXTURE_PROPERTY "HDRTexture"

struct TextureProperty : public Property
{
    std::string defaultPath;
    bool isHDRTexture;
};

#endif