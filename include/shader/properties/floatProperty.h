#ifndef FLOAT_PROPERTY_H
#define FLOAT_PROPERTY_H

#include "property.h"

#define TYPE_FLOAT_PROPERTY "Float"

struct FloatProperty : public Property
{
    float defaultValue;
};

#endif