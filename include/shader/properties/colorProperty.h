#ifndef COLOR_PROPERTY_H
#define COLOR_PROPERTY_H

#include "property.h"

#define TYPE_COLOR_PROPERTY "Color"

struct ColorProperty : public Property
{
    float defaultR;
    float defaultG;
    float defaultB;
    float defaultA;
};

#endif