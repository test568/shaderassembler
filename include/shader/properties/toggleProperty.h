#ifndef TOGGLE_PROPERTY_H
#define TOGGLE_PROPERTY_H

#include "property.h"

#define TYPE_TOGGLE_PROPERTY "Toggle"

struct ToggleProperty : public Property
{
    bool defaultValue;
    std::string define;
    int mask;
};

#endif