#ifndef INT_PROPERTY_H
#define INT_PROPERTY_H

#include "property.h"

#define TYPE_INT_PROPERTY "Int"

struct IntProperty : public Property
{
    int defaultValue;
};

#endif