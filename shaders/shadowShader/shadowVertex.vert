#version 330 core
layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec2 vUV;

uniform mat4 mxLightSpace;
uniform int typeAlpha = 1;

out vec2 uv;

void main()
{
    uv = vUV;
    gl_Position = mxLightSpace * vec4(vPosition, 1);
}