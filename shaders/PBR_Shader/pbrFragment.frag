#version 330
uniform mat4 mxProjection;
uniform mat4 mxPos;
uniform mat4 mxNormal;

struct directionL
{
    vec4 direction;
    vec4 color;
};

layout(std140) uniform directionLightBlock
{
    directionL directionLight;
};

uniform int countLights;

struct lightPoint
{
    vec4 color;
    vec4 point;
};

layout(std140) uniform lightPointBlock
{
    lightPoint lightPoints[9];
};

uniform sampler2D albedoMap;
uniform sampler2D normalMap;
uniform sampler2D metallic;
uniform sampler2D roughnessMap;
///

const float PI = 3.14159265359;

in VS_OUT {
    vec3 pos;
    vec2 uv;
    vec3 normal;
    vec3 Tpos;
    vec3 Tnormal;
    vec3 Tview;
    mat3 TBN;
} fs_in;

out vec4 colorF;

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / max(denom, 0.001);
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}


vec3 lightFromDirect(vec3 radiance, vec3 L, vec3 N, vec3 H, vec3 V, float metallic, vec3 albedo, float roughness, vec3 F0)
{
    float NDF = DistributionGGX(N, H, roughness);
    float G = GeometrySmith(N, V, L, roughness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0), F0);

    vec3 nominator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = nominator / max(denominator, 0.001);
        
    vec3 kS = F;

    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - metallic;

    float NdotL = max(dot(N, L), 0.0);

    return (kD * albedo / PI + specular) * radiance * NdotL;
}



void main()
{
    vec3 N = normalize((texture2D(normalMap, fs_in.uv).rgb * 2.0 - 1.0) + fs_in.Tnormal);

    vec3 V = normalize(-fs_in.pos);

    vec3 Lo = vec3(0);

    vec3 albedo = texture2D(albedoMap, fs_in.uv).rgb;
    float metallic = texture2D(metallic, fs_in.uv).r;
    float roughness = texture2D(roughnessMap, fs_in.uv).a;

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metallic);
    vec3 LtoSun = normalize(fs_in.TBN * directionLight.direction.xyz);

    Lo += lightFromDirect(directionLight.color.xyz * directionLight.color.a , LtoSun, N, normalize(V + LtoSun), V, metallic, albedo, roughness, F0);

    for(int i = 0; i < countLights; i++)
    {
        vec3 lightPos = fs_in.TBN * lightPoints[i].point.xyz;
        vec3 L = normalize(lightPos - fs_in.Tpos);
        vec3 H = normalize(V + L);
        float dist = distance(lightPos, fs_in.Tpos);
        float distMax = lightPoints[i].point.a;
        float intensivityLight = lightPoints[i].color.a;

        float attenuation = pow((distMax - dist) / distMax, 0.8);
        vec3 radiance = lightPoints[i].color.rgb * intensivityLight * attenuation;

        
        Lo += lightFromDirect(radiance, L, N, H, V, metallic, albedo, roughness, F0);
    }

    vec3 aimbent = vec3(0.13) * albedo;
    vec3 color = aimbent + Lo;
    color = color / (color + vec3(1.0)); 
    color = pow(color, vec3(1.0/2.2));
    colorF = vec4(vec3(color), 1);
}

