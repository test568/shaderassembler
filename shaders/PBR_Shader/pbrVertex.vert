#version 330

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vUV;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTanget;

uniform mat4 mxProjection;
uniform mat4 mxPos;
uniform mat4 mxNormal;

struct directionL
{
    vec4 direction;
    vec4 color;
};

layout(std140) uniform directionLightBlock
{
    directionL directionLight;
};


uniform int countLights;

struct lightPoint
{
    vec4 color;
    vec4 point;
};

layout(std140) uniform lightPointBlock
{
    lightPoint lightPoints[9];
};


out VS_OUT {
    vec3 pos;
    vec2 uv;
    vec3 normal;
    vec3 Tpos;
    vec3 Tnormal;
    vec3 Tview;
    mat3 TBN;
} vs_out;

void main()
{
    vec4 posBase = mxPos * vec4(vPosition, 1);
    vec4 normalBase = mxNormal * vec4(vNormal, 0);

    vec3 T = normalize(vec3(mxNormal * vec4(vTanget, 0)));
    vec3 B = normalize(vec3(mxNormal * vec4(cross(vNormal, vTanget), 0)));
    vec3 N = normalBase.xyz;

    mat3 TBN = transpose(mat3(T, B, N));
    vs_out.TBN = TBN;

    vs_out.pos = posBase.xyz;
    vs_out.uv = vUV;
    vs_out.normal = normalBase.xyz;

    vs_out.Tpos = TBN * vs_out.pos;
    vs_out.Tnormal = TBN * vs_out.normal;
    vs_out.Tview = TBN * -vs_out.pos;
    gl_Position = mxProjection * posBase;
}