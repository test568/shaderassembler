#ifdef FRAGMENT_SHADER

in VS_OUT {
    vec3 pos;
    vec2 uv;
    vec3 normal;
    vec3 view;
    mat3 TBN;
} fs_in;

#endif