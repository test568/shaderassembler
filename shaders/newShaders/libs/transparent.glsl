#ifdef FRAGMENT_SHADER

layout (binding = 0, r32ui) uniform uimage2D _trnspHeadsBuffer;
layout (binding = 1, r32i) uniform iimage2D _trnspCountUseBuffer;

layout (binding = 0, offset = 0) uniform atomic_uint _currentNodeND;

struct TrspNodeND
{
    vec4 normalDepth;
    uint next;
};

#ifdef DEPTH_NORMAL_PASS

    layout (binding = 0, std430) writeonly buffer _trnspNodesBuffer
    {
        TrspNodeND nodesND[];
    };

#else

    layout (binding = 0, std430) readonly buffer _trnspNodesBuffer
    {
        TrspNodeND nodesND[];
    };

#endif

#ifdef BASE_COLOR_PASS

    struct TrspNodeLight
    {
        vec4 pointAndDirectionLightsForce;
        vec4 lightPointsDirection;
    };

    layout (binding = 1, std430) readonly buffer _trnspNodesLightBuffer
    {
        TrspNodeLight nodesLight[];
    };

    struct TrspNodeResult
    {
        vec4 color;
    };

    layout (binding = 2, std430) writeonly buffer _trnspNodesResultBuffer
    {
        TrspNodeResult nodesResult[];
    };

#endif

uniform int _trnspMaxNodesPerPixel;
uniform int _trnspMaxNodes;

#ifdef DEPTH_NORMAL_PASS

uniform sampler2D _depthBuffer;
uniform ivec2 _viewSize;

bool trspPushDataDNPass(vec3 _normal, float _depth)
{
    ivec2 coord = ivec2(gl_FragCoord.xy);
    float curDepth =  texture(_depthBuffer, vec2(float(coord.x) / _viewSize.x,  float(coord.y) / _viewSize.y)).r;
    if(curDepth <= _depth)
    {
        discard;
    }

    int count = 0;
    count = imageAtomicAdd(_trnspCountUseBuffer, coord, int(-1)).r; 
    if(count <= 0)
    {
        return false;
    }

    uint nodeIndex = atomicCounterIncrement(_currentNodeND);
    uint prev = imageAtomicExchange(_trnspHeadsBuffer, coord, nodeIndex).r;

    nodesND[nodeIndex].next = prev;
    nodesND[nodeIndex].normalDepth = vec4(_normal, _depth);
    return true;
}

vec4 normalDepthTransparent();

void main()
{
    vec4 normalDepth = normalDepthTransparent();
    trspPushDataDNPass(normalDepth.rgb, normalDepth.a);
}

#endif

#ifdef BASE_COLOR_PASS

vec4 colorTransparent(vec4 PDLF, vec3 LPD);

float depthTransparent();

void main()
{
    float depth = depthTransparent();

    ivec2 coord = ivec2(gl_FragCoord.xy);
    uint index = 0;
    index = imageLoad(_trnspHeadsBuffer, coord).r;
    if(index == 0xFFFFFFFF || index >= _trnspMaxNodes)
    {
        return;
    }
    
    uint next = index;
    while(true)
    {
        vec4 data = nodesND[next].normalDepth;
        
        if(abs(depth - data.a) <= 0.0000001)
        {
            index = next;
            break;
        }

        next = nodesND[next].next;
        if(next == 0xFFFFFFFF)
        {
            index = 0xFFFFFFFF;
            break;
        }
    }

    if(index == 0xFFFFFFFF)
    {
        return;
    }
    else
    {
        nodesResult[index].color = colorTransparent(nodesLight[index].pointAndDirectionLightsForce, nodesLight[index].lightPointsDirection.rgb);
    }
}

#endif

#endif