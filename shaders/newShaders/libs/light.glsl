#ifdef FRAGMENT_SHADER

uniform vec3 _directionSun;
uniform vec4 _colorSun;
uniform float _intensity;

uniform vec3 _ambientLightColor;
uniform float _intensityAmbientLight;

uniform bool _environmentBufferAsAmbientLight;

uniform samplerCube _environmentBuffer;

uniform ivec2 _viewSize;

uniform sampler2D _pointAndDirectionLightsForceBuffer;
uniform sampler2D _lightPointsDirectionBuffer;

const float PI = 3.14159265359;

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a2 = roughness * roughness;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / max(denom, 0.001);
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float nom   = NdotV;
    float denom = NdotV * (1.0 - roughness) + roughness;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 lightFromDirect(vec3 radiance, vec3 L, vec3 N, vec3 H, vec3 V, float metallic, vec3 albedo, float roughness, vec3 F0)
{
    float NDF = DistributionGGX(N, H, roughness);
    float G = GeometrySmith(N, V, L, roughness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0), F0);
                
    vec3 nominator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = nominator / max(denominator, 0.001);

    vec3 kS = F;

    vec3 kD = vec3(1.0) - kS;
    
    kD *= 1.0 - metallic;
    
    float NdotL = max(dot(N, L), 0.0);
    return (kD * albedo / PI + specular) * radiance * NdotL;
}

vec3 lightFromMultiLightPoints(vec3 radiance, vec3 L, vec3 N, vec3 H, vec3 V, float metallic, vec3 albedo, float roughness, vec3 F0)
{
    float NDF = DistributionGGX(N, H, roughness);
    float G = GeometrySmith(N, V, L, roughness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0), F0);
                
    vec3 nominator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = nominator / max(denominator, 0.001);

    vec3 kS = F;

    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - metallic;
    
    return (kD * albedo / PI + specular) * radiance;
}

struct SurfaceData
{
    vec3 albedo;
    vec3 normal;
    float roughness;
    float metallic;
    float alpha;
};

vec4 PBRLight(SurfaceData surface, vec4 PDLF, vec3 LPD)
{
    float forceDirectionLight = PDLF.a;
    vec3 forceLightPoints = PDLF.rgb;
    vec3 directionLightPoints = normalize(LPD);

    vec3 V = normalize(fs_in.view);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, surface.albedo.rgb, surface.metallic);

    vec3 L0 = vec3(0);

    if(forceDirectionLight != 0)
    {
        L0 += lightFromDirect(_colorSun.rgb * _intensity * forceDirectionLight, _directionSun, surface.normal, normalize(V + _directionSun), V,
                                     surface.metallic, surface.albedo, surface.roughness, F0);
    }

    L0 += lightFromMultiLightPoints(forceLightPoints, directionLightPoints, surface.normal, normalize(V + directionLightPoints), V,
                                    surface.metallic, surface.albedo, surface.roughness, F0);


    vec3 dirAimbentLight = -reflect(V, surface.normal);
    if(_environmentBufferAsAmbientLight)
    {
        L0 += lightFromMultiLightPoints(mix(texture(_environmentBuffer, dirAimbentLight).rgb, _ambientLightColor, surface.roughness) * _intensityAmbientLight, dirAimbentLight, surface.normal, normalize(V + dirAimbentLight), V,
                                    surface.metallic, surface.albedo, surface.roughness, F0);
        L0 = max(L0, surface.albedo * _ambientLightColor * _intensityAmbientLight * 0.1);
    }
    else
    {
        L0 += surface.albedo * _ambientLightColor * _intensityAmbientLight;
    }

    return vec4(L0, surface.alpha);
}

vec4 PBRLight(SurfaceData surface)
{
    vec2 screenPos = vec2(gl_FragCoord.x / _viewSize.x, gl_FragCoord.y / _viewSize.y);
    vec4 PDLF = texture(_pointAndDirectionLightsForceBuffer, screenPos).rgba;
    vec3 LPD = texture(_lightPointsDirectionBuffer, screenPos).rgb;

    return PBRLight(surface, PDLF, LPD);
}

#endif