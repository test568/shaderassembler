#ifdef VERTEX_SHADER

out gl_PerVertex
{
    vec4 gl_Position;
};

out VS_OUT {
    vec3 pos;
    vec2 uv;
    vec3 normal;
    vec3 view;
    mat3 TBN;
} vs_out;

#endif
