#ifdef VERTEX_SHADER

layout(location = 0) in vec2 vPosition;
layout(location = 1) in vec3 vNormal;

uniform mat4 _matrix_P;

out gl_PerVertex
{
    vec4 gl_Position;
};

out vec3 normal;
out vec2 posInSide; 


void main()
{
    normal = normalize((_matrix_P * vec4(vNormal, 0)).xyz);
    gl_Position = vec4(vPosition, 0, 1);
}

#endif

#ifdef FRAGMENT_SHADER

uniform vec3 _directionSun;
uniform vec4 _colorSun;
uniform float _intensity;

in vec3 normal;
in vec2 posInSide; 

out vec4 fColor;

#endif