Shader = {
    Name = "StandardTransparent",
    Properties = {
        _MainTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _Color = {
            type = "Color",
            default = { r = 1, g = 1, b = 1, a = 1},
        },
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Transparent",
        RenderType = "Transparent",
        

        PassOne = {
            NamePass = "BasicPass",
            TypePass = "BasicColor",

            
            Includes = {
                "shaders/newShaders/libs/additionalDefines.glsl",
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/modelMatixs.vert",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/transparent.glsl",
                "shaders/newShaders/libs/light.glsl",
            },
            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
        DepthNormalPass = {
            NamePass = "DepthNormalPass",
            TypePass = "DepthNormal",
            
            Includes = {
                "shaders/newShaders/libs/additionalDefines.glsl",
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/modelMatixs.vert",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/transparent.glsl",
            },

            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        }
    }
}