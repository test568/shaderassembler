layout (std140, binding = 0) uniform matricesCamera
{
    mat4 matrixVP;
};

void main()
{
    vec4 posBase = _matrix_M * vec4(vPosition, 1);
    vec4 normalBase = _matrix_M * vec4(vNormal, 0);

    vec3 T = normalize(vec3(_matrix_M * vec4(vTanget, 0)));
    vec3 B = normalize(vec3(_matrix_M * vec4(cross(vNormal, vTanget), 0)));
    vec3 N = normalize(normalBase.xyz);

    mat3 TBN = transpose(mat3(T, B, N));
    vs_out.TBN = TBN;

    vs_out.pos = posBase.xyz;
    vs_out.uv = vUV;
    vs_out.normal = normalBase.xyz;
    vs_out.view = -normalize((_matrix_V * posBase).xyz);
    vs_out.view = (_matrixIverse_V * vec4(vs_out.view, 0)).xyz;

    gl_Position = _matrix_VP * posBase;
}