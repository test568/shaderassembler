out vec4 fcol;

#ifdef DEPTH_PASS

void main()
{
    fcol =  vec4(1, 1, 1, 1);
}

#endif

#ifdef SHADOW_PASS

void main()
{
    fcol =  vec4(1, 1, 1, 1);
}

#endif

#ifdef NORMAL_PASS

void main()
{
    fcol = vec4(fs_in.normal / 2 + vec3(0.5), 1);
}

#endif

#ifdef DEPTH_NORMAL_PASS

vec4 normalDepthTransparent()
{
    return vec4(fs_in.normal / 2 + vec3(0.5), gl_FragCoord.z);
}

#endif

#ifdef BASE_COLOR_PASS

uniform sampler2D _MainTex;
uniform vec4 _Color;
uniform int _T;

#ifdef RENDER_TYPE_TRANSPARENT

float depthTransparent()
{
    return gl_FragCoord.z;
}

vec4 colorTransparent(vec4 PDLF, vec3 LPD)
{  
    vec4 tex = texture2D(_MainTex, fs_in.uv) * _Color;
    SurfaceData surface;
    surface.albedo = tex.rgb;
    surface.metallic = 0.01;
    surface.roughness = 0.85;
    surface.alpha = tex.a;
    surface.normal = normalize(fs_in.normal);

    return PBRLight(surface, PDLF, LPD);
}

#else

void main()
{  
    vec3 tex = texture2D(_MainTex, fs_in.uv).rgb * _Color.rgb;
    SurfaceData surface;
    surface.albedo = tex.rgb;
    surface.metallic = 0.01;
    surface.roughness = 0.85;
    surface.alpha = 1;
    surface.normal = normalize(fs_in.normal);

    fcol = vec4(PBRLight(surface).rgb, 1);
}

#endif

#endif