Shader = {
    Name = "Standard",
    Properties = {
        _MainTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _BumpTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _T = {
            type = "Int",
            default = 1,
        },
        _Color = {
            type = "Color",
            default = { r = 1, g = 1, b = 1, a = 1},
        },
    },
    Settings = {
        NormalMap = "_BumpTex",
        ClippingMap = "_MainTex",
        ClippingChanel = A_Chanel,
        ClippingValue = 0.5,
        InverseClipping = false,
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "Opaque",
        GenerateInstancing = true,

        PassOne = {
            NamePass = "BasicPass",
            TypePass = "BasicColor",

            
            Includes = {
                "shaders/newShaders/libs/additionalDefines.glsl",
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/modelMatixs.vert",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/light.glsl",
            },
            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
        DepthPass = {
            NamePass = "DepthPass",
            TypePass = "Depth",
            
            UseDefaultPrograms = true,
            UseNormalMap = true,
            UseClippingMap = true,
        },
        NormalPass = {
            NamePass = "NormalPass",
            TypePass = "Normal",
            
            Includes = {
                "shaders/newShaders/libs/additionalDefines.glsl",
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/modelMatixs.vert",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag"
            },

            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
        ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",
            
            Includes = {
                "shaders/newShaders/libs/additionalDefines.glsl",
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/modelMatixs.vert",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag"
            },

            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
    }
}