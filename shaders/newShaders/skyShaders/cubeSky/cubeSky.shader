Shader = {
    Name = "CubeSky",
    Properties = {
        _SkyBox = {
            type = "CubeMap",
        },
    },
    SubShader = {
        SkyPass = {
            NamePass = "SkyPass",
            TypePass = "BasicColor",

            Includes = {
                "shaders/newShaders/libs/sky.glsl",
            },
            Vertex = "shaders/newShaders/skyShaders/cubeSky/vert.vert",
            Fragment = "shaders/newShaders/skyShaders/cubeSky/frag.frag",
        },
    }
}