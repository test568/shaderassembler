uniform samplerCube _SkyBox;

void main()
{
    vec3 N = normalize(normal);
    fColor = vec4(texture(_SkyBox, N).rgb, 1);
}   