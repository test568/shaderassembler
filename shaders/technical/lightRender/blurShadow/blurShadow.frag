#version 430
layout(location = 0) out vec4 result;

in vec2 uv;

uniform sampler2D _source;

uniform float _stepSmoothing;

void main()
{
    float average = 0;
    vec2 texelSize = 1.0 / textureSize(_source, 0);
    for(int i = -1; i < 1; i++)
    {
        for(int i2 = -1; i2 < 1; i2++)
        {
            average += texture(_source, uv + vec2(texelSize.x * i, texelSize.y * i2)).r;
        }
    }
    average /= 4;
    /*if(average < 1)
    {*/
        average *= _stepSmoothing;
    //}
    result = vec4(average, 0, 0, 0);
    //result = vec4(texture(_source, uv).r, 0, 0, 0);
}