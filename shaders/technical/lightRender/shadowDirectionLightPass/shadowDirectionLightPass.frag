#version 430
layout(location = 0) out vec4 pointAndDirectionLightsForce;

in vec2 uv;

uniform sampler2D depthBuffer;

uniform mat4 matrixIverse_P;
uniform mat4 matrixIverse_V;

uniform int _countPartition;
uniform mat4 _shadowMatrix_VP[8];
uniform vec2 _shadowClipDistances[8];
uniform int _countSubMaps[8];
uniform sampler2D _shadowMaps[31];

uniform float _decayDistance;
uniform float _maxDecay;


vec3 getPosition(float depth)
{
    depth = depth * 2.0 - 1.0;

    vec4 clipSpacePosition = vec4(vec2(uv * 2.0 - 1.0), depth, 1.0);
    vec4 viewSpacePosition = matrixIverse_P * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    vec4 worldSpacePosition = matrixIverse_V * viewSpacePosition;
    return worldSpacePosition.xyz;
}

float getForceShadow(sampler2D _shadow, vec3 _pos, vec2 _distances)
{
    vec2 texelSize = 1.0 / textureSize(_shadow, 0);
    float offset = 0.000005;
    float res = 0;
    int count  = 0;
    for(float i = -3; i < 3; i += 0.75)
    {
        for(float i2 = -3; i2 < 3; i2 += 0.75)
        {
            vec2 p = _pos.xy + vec2(texelSize.x * i, texelSize.y * i2);
            if(float(fract( abs((i + i2) * 0.5))) > 0.25)
            {
                continue;
            }
            count++;

            float mapDepth = texture(_shadow, p).r;
            if(_pos.z - offset > mapDepth)
            {
                float dist = (_distances.x + _pos.z * _distances.y) - (_distances.x + mapDepth * _distances.y);
                res += 1 - min(pow(min(dist / _decayDistance, 1), 1.0 / 2.2), _maxDecay);
            }
        }
    }
    if(count == 0)
    {
        return 0;
    }
    return res / count;
}

float getForceShadow(vec3 position)
{
    int numTexture = 0;
    int endOn = _countPartition;

    float forceShadow = 0;

    for(int i = 0; i < endOn; i++)
    {
        vec3 posInShadow = (_shadowMatrix_VP[i] * vec4(position, 1)).xyz;
        if(abs(posInShadow.x) >= 1 || abs(posInShadow.y) >= 1 || abs(posInShadow.z) >= 1 ) 
        {
            numTexture += _countSubMaps[i];
            continue;
        }
        
        posInShadow = posInShadow * 0.5 + vec3(0.5);
        endOn = i;

        forceShadow = getForceShadow(_shadowMaps[numTexture], posInShadow, _shadowClipDistances[i]);
        
        if(forceShadow > 0)
        {
            for(int i2 = 1; i2 < _countSubMaps[i]; i2++)
            {
                forceShadow += getForceShadow(_shadowMaps[numTexture + i2], posInShadow, _shadowClipDistances[i]);
            }
        }
        forceShadow /= _countSubMaps[i];
        numTexture += _countSubMaps[i];
    }

    return clamp(forceShadow, 0, 1);
}

void main()
{
    float depth = texture(depthBuffer, uv).r;
    if(depth == 1.0)
    {
        pointAndDirectionLightsForce = vec4(1, 0, 0, 0);
        return;
    }
    vec3 position = getPosition(depth);

    float forceShadow = getForceShadow(position);
    pointAndDirectionLightsForce = vec4(1 - forceShadow, 0, 0, 0);
}