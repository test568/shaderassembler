#version 430
layout(location = 0) out vec4 pointAndDirectionLightsForce;
layout(location = 1) out vec4 lightPointsDirection;

in vec2 uv;

uniform sampler2D depthBuffer;
uniform sampler2D normalBuffer;
uniform sampler2D shadowDirectionLight;

uniform mat4 matrixIverse_P;
uniform mat4 matrixIverse_V;

struct lightPointData
{
    vec3 position;                  //16        0
    vec3 color;                     //16        16
    float range;                    //4         28
    float intensivity;              //4         32 
    float near;                     //4         36
    float far;                      //4         40
    bool useShadow;                 //4         44
    mat4 matrix_P;                  //16        48
    mat4 matrixInverse_P;           //16        112
    mat4 matrix_V[6];               //16        176 + 6 * 64
    mat4 matrixIverse_V[6];         //16        560 + 6 * 64
}; // 944

uniform int countLightPoints;

uniform samplerCube shadowsLightPoints[16];

layout(std140) uniform lightPointBlock
{
    lightPointData lightPoints[16];
};



layout (binding = 0, r32ui) uniform uimage2D _trnspHeadsBuffer;

uniform int _trnspMaxNodes;

struct TrspNodeND
{
    vec4 normalDepth;
    uint next;
};

layout (binding = 0, std430) readonly buffer _trnspNodesBuffer
{
    TrspNodeND nodesND[];
};

struct TrspNodeLight
{
    vec4 pointAndDirectionLightsForce;
    vec4 lightPointsDirection;
};

layout (binding = 1, std430) writeonly buffer _trnspNodesLightBuffer
{
    TrspNodeLight nodesLight[];
};

vec3 getPosition(float depth)
{
    depth = depth * 2.0 - 1.0;

    vec4 clipSpacePosition = vec4(vec2(uv * 2.0 - 1.0), depth, 1.0);
    vec4 viewSpacePosition = matrixIverse_P * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    vec4 worldSpacePosition = matrixIverse_V * viewSpacePosition;
    return worldSpacePosition.xyz;
}

vec3 getNormal()
{
    return (texture(normalBuffer, uv).rgb - 0.5) * 2.0;
}

float getLightShadowForce(int index, vec3 position, vec2 offsetDir)
{
    if(lightPoints[index].useShadow == true)
    {
        float offset = 0.00005;
        for(int i = 0; i < 6; i++)
        {
            vec4 posInShadow = (vec4(position, 1) * lightPoints[index].matrix_V[i]);
            vec4 posInProject = (posInShadow * lightPoints[index].matrix_P);
            if(abs(posInProject.x / posInProject.w) >= 1 || abs(posInProject.y / posInProject.w) >= 1 || abs(posInProject.z / posInProject.w) >= 1) 
            {
                continue;
            }

            vec3 dir = normalize((position - lightPoints[index].position));
            if(abs(dot(dir, vec3(0, 1, 0))) >= 0.7)
            {
                vec3 side = normalize(cross(dir, vec3(0, 0, 1)));
                vec3 up = normalize(cross(dir, side));
                dir = normalize(dir + (side * offsetDir.x) + (up * offsetDir.y));
            }
            else
            {
                vec3 side = normalize(cross(dir, vec3(0, 1, 0)));
                vec3 up = normalize(cross(dir, side));
                dir = normalize(dir + (side * offsetDir.x) + (up * offsetDir.y));
            }

            float depthShadow = texture(shadowsLightPoints[index], dir).r * 2.0 - 1.0;

            if(posInProject.z / posInProject.w - offset / posInProject.w >= depthShadow)
            {
                return 1;
            }
        }
    }
    return 0;
}

struct LightData 
{
    vec4 pointAndDirectionLightsForce;
    vec4 lightPointsDirection;
};

LightData getLightData(vec3 normal, vec3 position, bool generateShadows)
{
    LightData data;

    vec3 sumColorLights = vec3(0);
    vec3 sumLightPointsDirections = vec3(0);

    float sumIntensivity = 0;

    int countUseLights = 0;
    for(int i = 0; i < countLightPoints; i++)
    {
        vec3 pL = lightPoints[i].position;
        vec3 cL = lightPoints[i].color;
        float range = lightPoints[i].range;
        float intensivity = lightPoints[i].intensivity;

        vec3 dir = pL - position;
        float dist = length(dir);
        dir = normalize(dir);
        float dotND = dot(dir, normal);
        if(dist > range || dotND <= 0)
        {
            continue;
        }
        countUseLights++;

        float shadowForce = 0;
        int countShadows = 0;
        if(generateShadows)
        {
            for(int i = -1; i < 1; i++)
            {
                for(int i2 = -1; i2 < 1; i2++)
                {
                    countShadows++;
                    shadowForce += getLightShadowForce(i, position, vec2(float(i) / 150, float(i2) / 150));
                }
            }
        }
        else
        {
            shadowForce = 0;
            countShadows = 1;
        }

        float finalIntensivity = mix( ((range - dist) / range) * intensivity * dotND, 0, shadowForce / countShadows );

        sumIntensivity += finalIntensivity;
        sumColorLights += cL * finalIntensivity;
        sumLightPointsDirections += dir * finalIntensivity;
    }

    if(generateShadows)
    {
        data.pointAndDirectionLightsForce = vec4(sumColorLights, texture(shadowDirectionLight, uv).r);
    }
    else
    {
        data.pointAndDirectionLightsForce = vec4(sumColorLights, 1);
    }
    data.lightPointsDirection = vec4(normalize(sumLightPointsDirections), 1);

    return data;
}

void main()
{
    ivec2 coord = ivec2(gl_FragCoord.xy);
    uint index = 0;
    index = imageLoad(_trnspHeadsBuffer, coord).r;
    if(index != 0xFFFFFFFF)
    {
        uint next = index;
        while(true)
        {
            vec4 data = nodesND[next].normalDepth;
            vec3 normal = (data.rgb - 0.5) * 2.0;
            vec3 position = getPosition(data.a);

            LightData light = getLightData(normal, position, false);

            nodesLight[next].pointAndDirectionLightsForce = light.pointAndDirectionLightsForce;
            nodesLight[next].lightPointsDirection = light.lightPointsDirection;

            next = nodesND[next].next;
            if(next == 0xFFFFFFFF)
            {
                break;
            }
        }
    }

    float depth = texture(depthBuffer, uv).r;
    if(depth == 1.0)
    {
        return;
    }

    vec3 normal = getNormal();
    vec3 position = getPosition(depth);

    LightData mainLight = getLightData(normal, position, true);

    pointAndDirectionLightsForce = mainLight.pointAndDirectionLightsForce;
    lightPointsDirection = mainLight.lightPointsDirection;
}