#version 430

in vec2 uv;

uniform sampler2D _mainTex;

out vec4 outColor;

void main()
{
    outColor = texture(_mainTex, uv).rgba;
}