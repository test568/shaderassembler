#version 430

in vec2 uv;

uniform sampler2D _pointAndDirectionLightsForce;
uniform sampler2D _lightPointsDirection;

out vec4 outColor;

void main()
{
    float alpha = texture(_pointAndDirectionLightsForce, uv).a; 
    outColor = vec4(vec3(alpha), alpha);
}