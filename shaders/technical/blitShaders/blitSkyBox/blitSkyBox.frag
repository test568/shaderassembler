#version 430

in vec2 uv;
in vec3 normal;

uniform samplerCube skyBox;

out vec4 outColor;

void main()
{
    vec3 N = normalize(normal);
    outColor = texture(skyBox, N);
}