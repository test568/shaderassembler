#version 430

in vec2 uv;

uniform sampler2D _skyBoxBuffer;
uniform sampler2D _basicColorBuffer;
uniform sampler2D _depthBuffer;
uniform sampler2D _debugLinesBuffer;

layout (binding = 0, r32ui) uniform uimage2D _trnspHeadsBuffer;

struct TrspNodeND
{
    vec4 normalDepth;
    uint next;
};

layout (binding = 0, std430) readonly buffer _trnspNodesBuffer
{
    TrspNodeND nodesND[];
};


struct TrspNodeResult
{
    vec4 color;
};

layout (binding = 2, std430) readonly buffer _trnspNodesResultBuffer
{
    TrspNodeResult nodesResult[];
};


struct Frag
{
    vec4 color;
    float depth;
};

out vec4 outColor;

vec4 getBasicColor()
{
    vec4 color = texture(_basicColorBuffer, uv);

    Frag frags[9];
    int count = 0;

    ivec2 coord = ivec2(gl_FragCoord.xy);
    uint index = 0;
    index = imageLoad(_trnspHeadsBuffer, coord).r;

    while ( index != 0xFFFFFFFF && count < 9 )
    {
        frags[count].color = nodesResult[index].color;
        frags[count].depth = nodesND[index].normalDepth.a;
        index = nodesND[index].next;
        count++;
    }

    for ( int i = 1; i < count; i++ )
    {
        Frag toInsert = frags[i];
        uint j = i;
        
        while (j > 0 && toInsert.depth > frags[j-1].depth )
        {
            frags[j] = frags[j-1];
            j--;
        }
        
        frags[j] = toInsert;
    }
    
    for (int i = 0; i < count; i++)
    {
        color = mix(color, frags[i].color, frags[i].color.a);
    }

    return color;
}

void main()
{
    vec4 sb = texture(_skyBoxBuffer, uv);
    vec4 bc = getBasicColor();
    
    vec3 result = mix(sb, bc, bc.a).rgb;
    result = clamp(result, vec3(0), vec3(1));
    result = result / (result + vec3(1)); 
    result = pow(result, vec3(1.0/2.2));
    outColor = vec4(result, 1);
}