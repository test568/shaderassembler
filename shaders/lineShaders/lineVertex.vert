#version 330



layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vUV;
layout(location = 2) in vec3 vNormal;

uniform mat4 mxProjection;
uniform mat4 mxPos;
uniform mat4 mxNormal;

uniform vec3 light_pos = vec3(100.0, 100.0, 100.0);

out vec3 vCol;

out float TexCoord;

out VS_OUT {
 vec3 normal;
 vec3 view;
 vec3 light;
} vs_out;

void main()
{
    vec4 posBase = mxPos * vec4(vPosition, 1);
    vec4 normalBase = mxNormal * vec4(vNormal, 1);

    vs_out.view = -posBase.xyz;
    vs_out.normal = normalBase.xyz;

    vs_out.light = light_pos - posBase.xyz;

    vCol = vec3(1, 0, 1);
    gl_Position = mxProjection * posBase;

    TexCoord = vUV.t;
}