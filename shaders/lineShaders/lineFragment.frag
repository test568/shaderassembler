#version 330

layout (location = 0) out vec4 color;

uniform mat4 mxProjection;
uniform mat4 mxPos;
uniform mat4 mxNormal;

uniform vec3 light_pos = vec3(100.0, 100.0, 100.0);

in vec3 vCol;

in float TexCoord;

in VS_OUT {
    vec3 normal;
    vec3 view;
    vec3 light;
} fs_in;

void main()
{
    vec3 N = normalize(fs_in.normal);
    vec3 L = normalize(fs_in.light);
    vec3 V = normalize(fs_in.view);
    
    vec3 skyColor = vec3(0.65, 0.84, 0.8) * 1.3;
    vec3 groundColor = vec3(0.23, 0.14, 0.04);

    float dotSky = dot(vec3(0, 1, 0), N);
    dotSky = (dotSky + 1) / 2;
    dotSky -= 0.9;
    dotSky =  clamp(tan(dotSky) + 0.9, 0, 1);

    vec3 ref = reflect(-L, N);

    vec3 difuce = (skyColor * dotSky + (1 - dotSky) * groundColor) * vCol;
    vec3 specular = pow(max(dot(ref, V), 0), 20) * vec3(0.8);

    //color = vec4(vec3(0.1) + difuce + specular, 1.0);

    float size = 5;
    float width = 1.2;
    float fuzz = 0.1;

    float nor1 = fract(TexCoord * size * 2);
    float nor2 = fract(TexCoord * size);

    float res = abs(step(0.5, nor2)-nor1);
    res = clamp(res * (width) - fuzz, 0, 1);
    
    color = vec4(vec3(res), 1.0);

}