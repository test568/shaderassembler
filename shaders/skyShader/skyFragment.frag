#version 430 core

layout(location = 0) in vec2 point;
layout(location = 1) in vec3 direct;

uniform vec3 dirCamera;
uniform vec3 dirSun;
uniform vec3 colorSky;
uniform vec3 colorGround;
uniform vec3 colorSun;
uniform vec3 colorEdgeHorizont;
uniform float rangeBlackoutCameraSky = 1;
uniform float rangeBlackoutSunSky = 0.2;

in vec3 directToWorld;

out vec4 vColor;

float getIntesivitySunForSize(float size, float dotSun)
{
   return pow(1.3 * pow(dotSun, 10 / size), 5.5 );
}

void main()
{
   float sizeSun = 0.0198;

   vec3 D = normalize(directToWorld);

   float dotUp = dot(vec3(0, 1, 0), D);
   float dotSun = clamp(dot(dirSun, D), 0, 1);
   float dotSunToCenterCamera = clamp(dot(dirCamera, dirSun), 0 , 1);


   float stateSun = dot(vec3(0, 1, 0), dirSun);
   stateSun = clamp(stateSun, 0, 1);
   
   //back

   float blackoutSky = max(0, 1 - pow(( 1 - stateSun) * rangeBlackoutCameraSky,2) - pow((1 - clamp(dot(dirCamera, dirSun), 0, 1)) * rangeBlackoutSunSky, 2 ));
   float skyCoef = dotUp;
   skyCoef = clamp(skyCoef, 0, 1);
   skyCoef = pow(skyCoef, 0.2);
   vec3 colorBack = colorGround * (1 - skyCoef) + colorSky * skyCoef * blackoutSky;

   //sun
   float intesity = getIntesivitySunForSize(sizeSun, dotSun);
   intesity = intesity * ( 1 - step(dotUp,0));
   vec3 sun = colorSun * intesity;

   //sun blum effect 
   vec3 intesityBlum =  pow(dotSun * dotSunToCenterCamera, 128) * colorSun * 0.2;
   intesityBlum += sqrt(dotSun * dotSun +  dotSunToCenterCamera * dotSunToCenterCamera) * 0.2 * colorSun;

   //horizontal
   float horizontLight = 1 - abs(dotUp);
   horizontLight = pow(horizontLight, 10) * 0.8;

   float horizontSun = horizontLight + pow(pow(horizontLight, 4.9) * pow(intesity, 1.6), 0.1);
   horizontSun = horizontSun + intesity * 0.4;


   vec3 light =  mix(horizontSun * colorEdgeHorizont + intesity * colorSun, vec3(horizontLight) + intesity * colorSun, stateSun) * 0.8;
   colorBack = mix(colorBack, light, horizontLight * 0.4); 
   colorBack = mix(colorBack, light, horizontSun  * 0.4); 
   colorBack += light * stateSun * 0.4;
   colorBack += intesityBlum;
   colorBack = pow(colorBack, vec3(1.0/2.2));
   vColor = vec4(vec3(colorBack), 1);
}