#version 430 core

layout(location = 0) in vec2 point;
layout(location = 1) in vec3 direct;

uniform vec3 dirCamera;
uniform vec3 dirSun;
uniform vec3 colorSky;
uniform vec3 colorGround;
uniform vec3 colorSun;
uniform vec3 colorEdgeHorizont;
uniform float rangeBlackoutCameraSky;
uniform float rangeBlackoutSunSky;
out vec3 directToWorld;

void main()
{
    directToWorld = direct;
    gl_Position = vec4(point.xy, 0, 1);
}