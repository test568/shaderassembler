#version 430 core

layout(location = 0) in vec3 vPosition;

uniform mat4 mxPosProjection;
uniform vec3 color;

in vec4 vPos;

out vec4 vColor;

void main()
{
   vColor = vec4(color, 1);
}