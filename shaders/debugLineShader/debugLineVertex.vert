#version 430 core

layout(location = 0) in vec3 vPosition;

uniform mat4 mxPosProjection;
uniform vec3 color;

out vec4 vPos;

void main()
{

    vPos = mxPosProjection * vec4(vPosition, 1);
    gl_Position = vPos;
}