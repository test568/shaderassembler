uniform int countLights;

struct lightPoint
{
    vec4 color;
    vec4 point;
};

layout(std140) uniform lightPointBlock
{
    lightPoint lightPoints[9];
};