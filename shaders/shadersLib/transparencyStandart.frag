
uniform float basicTransparency_cutoutlevel = 0.5;

subroutine vec4 alpha (vec4 colorIn);

subroutine (alpha)
    vec4 basicTransparency_T_F_O( vec4 colorIn ) { return colorIn; }; 

subroutine (alpha) 
    vec4 basicTransparency_T_F_C( vec4 colorIn ) 
    { 
        if(colorIn.a < basicTransparency_cutoutlevel)
        {
            discard;
        }
        return colorIn;
    };

subroutine (alpha)
    vec4 basicTransparency_T_F_T( vec4 colorIn ) { return colorIn; }; 

subroutine uniform alpha basicTransparency_T_S;
