struct directionL
{
    vec4 direction;
    vec4 color;
};

layout(std140) uniform directionLightBlock
{
    directionL directionLight;
};

uniform mat4 mxDirectionLightSpace;
uniform sampler2D directionLightShadowMap;

float shadowDirectionLight(vec3 pos)
{
    if(abs(pos.x) > 1 || abs(pos.y) > 1 || abs(pos.z) > 1 ) return 0;

    float shadow = 0;
    float offset = 0.00005;
    pos = pos * 0.5 + vec3(0.5);

    vec2 texelSize = 1.0 / textureSize(directionLightShadowMap, 0);

    for(int x = -1; x <= 1; x++)
    {
        for(int y = -1; y <= 1; y++)
        {
            float mapDepth = texture2D(directionLightShadowMap, pos.xy + vec2(x, y) * texelSize).r;
            shadow += pos.z - offset > mapDepth ? 1.0 : 0.0;
        }
    }
    return shadow / 9.0;
}