vec3 unpackNormal(vec3 rgb, vec3 addNormal)
{
    return normalize(rgb * 2.0 - vec3(1.0) + addNormal);
}