vec2 convertUV(vec2 uv)
{
    return vec2(uv.x, 1 - uv.y);
}