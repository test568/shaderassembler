out VS_OUT {
    vec3 pos;
    vec2 uv;
    vec3 normal;
    vec3 Tpos;
    vec3 Tnormal;
    vec3 Tview;
    mat3 TBN;

    vec3 posInDirectionLight;
} vs_out;


