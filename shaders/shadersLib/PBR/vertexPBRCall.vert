
#include<modelLayers.vert>
#include<modelMatixs.vert>
#include<directionLightUNF.vert>
#include<lightPointsB.vert>
#include<PBR/vertexOutBasic.vert>

void main()
{

    vec4 posBase = mxPos * vec4(vPosition, 1);
    vec4 normalBase = mxPos * vec4(vNormal, 0);

    vec3 T = normalize(vec3(mxPos * vec4(vTanget, 0)));
    vec3 N = normalize(normalBase.xyz);
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);
   

    mat3 TBN = transpose(mat3(T, B, N));
    vs_out.TBN = TBN;

    vs_out.posInDirectionLight = (mxDirectionLightSpace * vec4(vPosition, 1)).xyz;

    vs_out.pos = posBase.xyz;
    vs_out.uv = vUV;
    vs_out.normal = normalBase.xyz;

    vs_out.Tpos = TBN * vs_out.pos;
    vs_out.Tnormal =  normalize(TBN * vs_out.normal);
    vs_out.Tview = TBN * normalize(-vs_out.pos);
    gl_Position = mxProjection * posBase;
}