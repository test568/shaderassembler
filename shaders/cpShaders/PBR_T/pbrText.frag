#version 430
#if __VERSION__ < 130
#define TEXTURE2D texture2D
#else
#define texture2D texture
#endif


#include<PBR/fragmentPBRCall.frag>
#include<utility/unpackNormal.frag>
#include<transparencyStandart.frag>

uniform sampler2D albedoMap;
uniform sampler2D normalMap;
uniform sampler2D metallic;
uniform sampler2D roughnessMap;


PBR_FRAG_OUT fragmentPBR()
{
    PBR_FRAG_OUT outFrag;  
    vec2 uv = vec2( fs_in.uv.x, 1 - fs_in.uv.y);
    outFrag.albedo = basicTransparency_T_S(vec4(texture2D(albedoMap, uv ).rgba));
    //outFrag.albedo = vec4(vec3(texture2D(directionLightShadowMap, uv).rgb ), 1);
    outFrag.metallic = texture2D(metallic, uv).r;
    outFrag.roughness = texture2D(roughnessMap, uv).a;
    outFrag.normal = unpackNormal(texture2D(normalMap, uv).rgb, fs_in.Tnormal);
    return outFrag;
}