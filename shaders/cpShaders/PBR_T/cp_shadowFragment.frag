#version 330 core

vec2 convertUV(vec2 uv)
{
    return vec2(uv.x, 1 - uv.y);
}

uniform mat4 mxLightSpace;

uniform int typeAlpha = 1;
uniform sampler2D mainTex;


in vec2 uv;

void main()
{
    if(typeAlpha == 1)
    {
        gl_FragDepth = gl_FragCoord.z;
        return;
    }
    else if(typeAlpha == 2)
    {
        if(texture2D(mainTex, uvDecoder(uv)).a < 0.5)
        {
            discard;
        }
        else
        {  
            gl_FragDepth = gl_FragCoord.z;
            return;
        }
    }
    else if(typeAlpha == 3)
    {
        if(texture2D(mainTex, uvDecoder(uv)).a != 1.0)
        {
            discard;
        }
        else
        {  
            gl_FragDepth = gl_FragCoord.z;
            return;
        }
    }
}