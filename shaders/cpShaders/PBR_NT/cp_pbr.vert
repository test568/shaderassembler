#version 430
#if __VERSION__ < 130
#define TEXTURE2D texture2D
#else
#define texture2D texture
#endif



uniform float basicTransparency_cutoutlevel = 0.5;


layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vUV;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTanget;
uniform mat4 mxProjection;
uniform mat4 mxPos;
uniform mat4 mxNormal;
struct directionL
{
    vec4 direction;
    vec4 color;
};

layout(std140) uniform directionLightBlock
{
    directionL directionLight;
};

uniform mat4 mxDirectionLightSpace;
uniform sampler2D directionLightShadowMap;

float shadowDirectionLight(vec3 pos)
{
    if(abs(pos.x) > 1 || abs(pos.y) > 1 || abs(pos.z) > 1 ) return 0;

    float shadow = 0;
    float offset = 0.00005;
    pos = pos * 0.5 + vec3(0.5);

    vec2 texelSize = 1.0 / textureSize(directionLightShadowMap, 0);

    for(int x = -1; x <= 1; x++)
    {
        for(int y = -1; y <= 1; y++)
        {
            float mapDepth = texture2D(directionLightShadowMap, pos.xy + vec2(x, y) * texelSize).r;
            shadow += pos.z - offset > mapDepth ? 1.0 : 0.0;
        }
    }
    return shadow / 9.0;
}
uniform int countLights;

struct lightPoint
{
    vec4 color;
    vec4 point;
};

layout(std140) uniform lightPointBlock
{
    lightPoint lightPoints[9];
};
out VS_OUT {
    vec3 pos;
    vec2 uv;
    vec3 normal;
    vec3 Tpos;
    vec3 Tnormal;
    vec3 Tview;
    mat3 TBN;

    vec3 posInDirectionLight;
} vs_out;




void main()
{

    vec4 posBase = mxPos * vec4(vPosition, 1);
    vec4 normalBase = mxPos * vec4(vNormal, 0);

    vec3 T = normalize(vec3(mxPos * vec4(vTanget, 0)));
    vec3 N = normalize(normalBase.xyz);
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);
   

    mat3 TBN = transpose(mat3(T, B, N));
    vs_out.TBN = TBN;

    vs_out.posInDirectionLight = (mxDirectionLightSpace * vec4(vPosition, 1)).xyz;

    vs_out.pos = posBase.xyz;
    vs_out.uv = vUV;
    vs_out.normal = normalBase.xyz;

    vs_out.Tpos = TBN * vs_out.pos;
    vs_out.Tnormal =  normalize(TBN * vs_out.normal);
    vs_out.Tview = TBN * normalize(-vs_out.pos);
    gl_Position = mxProjection * posBase;
}