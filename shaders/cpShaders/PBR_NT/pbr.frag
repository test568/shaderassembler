#version 430
#if __VERSION__ < 130
#define TEXTURE2D texture2D
#else
#define texture2D texture
#endif


#include<PBR/fragmentPBRCall.frag>
#include<utility/unpackNormal.frag>
#include<transparencyStandart.frag>

PBR_FRAG_OUT fragmentPBR()
{
    PBR_FRAG_OUT outFrag;  
    outFrag.albedo = basicTransparency_T_S(vec4(1, 1, 1, 1));
    outFrag.metallic = 0.1;
    outFrag.roughness = 0.9;
    outFrag.normal = fs_in.Tnormal;
    return outFrag;
}