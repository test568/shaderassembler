#version 430
#if __VERSION__ < 130
#define TEXTURE2D texture2D
#else
#define texture2D texture
#endif



uniform mat4 mxProjection;
uniform mat4 mxPos;
uniform mat4 mxNormal;
struct directionL
{
    vec4 direction;
    vec4 color;
};

layout(std140) uniform directionLightBlock
{
    directionL directionLight;
};

uniform mat4 mxDirectionLightSpace;
uniform sampler2D directionLightShadowMap;

float shadowDirectionLight(vec3 pos)
{
    if(abs(pos.x) > 1 || abs(pos.y) > 1 || abs(pos.z) > 1 ) return 0;

    float shadow = 0;
    float offset = 0.00005;
    pos = pos * 0.5 + vec3(0.5);

    vec2 texelSize = 1.0 / textureSize(directionLightShadowMap, 0);

    for(int x = -1; x <= 1; x++)
    {
        for(int y = -1; y <= 1; y++)
        {
            float mapDepth = texture2D(directionLightShadowMap, pos.xy + vec2(x, y) * texelSize).r;
            shadow += pos.z - offset > mapDepth ? 1.0 : 0.0;
        }
    }
    return shadow / 9.0;
}
uniform int countLights;

struct lightPoint
{
    vec4 color;
    vec4 point;
};

layout(std140) uniform lightPointBlock
{
    lightPoint lightPoints[9];
};

in VS_OUT {
    vec3 pos;
    vec2 uv;
    vec3 normal;
    vec3 Tpos;
    vec3 Tnormal;
    vec3 Tview;
    mat3 TBN;

    vec3 posInDirectionLight;
} fs_in;

const float PI = 3.14159265359;

out vec4 colorF;

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / max(denom, 0.001);
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}


vec3 lightFromDirect(vec3 radiance, vec3 L, vec3 N, vec3 H, vec3 V, float metallic, vec3 albedo, float roughness, vec3 F0)
{
    float NDF = DistributionGGX(N, H, roughness);
    float G = GeometrySmith(N, V, L, roughness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0), F0);
                
    vec3 nominator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = nominator / max(denominator, 0.001);

    vec3 kS = F;

    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - metallic;
    
    float NdotL = max(dot(N, L), 0.0);
    return (kD * albedo / PI + specular) * radiance * NdotL;
}

struct PBR_FRAG_OUT
{
    vec4 albedo;
    vec3 normal;
    float roughness;
    float metallic;
};

PBR_FRAG_OUT fragmentPBR();

void main()
{
    PBR_FRAG_OUT outCall = fragmentPBR();

    vec4 albedo = outCall.albedo;
    float metallic = outCall.metallic;
    float roughness = outCall.roughness;
    vec3 N = outCall.normal;

    vec3 V = normalize(fs_in.Tview);

    vec3 Lo = vec3(0);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo.xyz, metallic);
    vec3 LtoSun = normalize(fs_in.TBN * directionLight.direction.xyz);

    Lo += lightFromDirect(directionLight.color.xyz * directionLight.color.a , LtoSun, 
    N, normalize(V + LtoSun), V, metallic, albedo.xyz, roughness, F0) * (1.0 - shadowDirectionLight(fs_in.posInDirectionLight));


    for(int i = 0; i < countLights; i++)
    {
        vec3 lightPos = fs_in.TBN * lightPoints[i].point.xyz;
        vec3 L = normalize(lightPos - fs_in.Tpos);

        vec3 H = normalize(L + V);
            
        float dist = distance(lightPos, fs_in.Tpos);
        float distMax = lightPoints[i].point.a;
        if(dist > distMax) continue;
        float intensivityLight = lightPoints[i].color.a;
        
        float attenuation = pow((distMax - dist) / distMax, 0.8);
        vec3 radiance = lightPoints[i].color.rgb * intensivityLight * attenuation;

        Lo += lightFromDirect(radiance, L, N, H, V, metallic, albedo.xyz, roughness, F0);
    }

    vec3 aimbent = vec3(0.13) * albedo.xyz;
    vec3 color = aimbent + Lo;
    color = color / (color + vec3(1.0)); 
    color = pow(color, vec3(1.0/2.2));
    colorF = vec4(vec3(color), albedo.a);
}
vec3 unpackNormal(vec3 rgb, vec3 addNormal)
{
    return normalize(rgb * 2.0 - vec3(1.0) + addNormal);
}

uniform float basicTransparency_cutoutlevel = 0.5;

subroutine vec4 alpha (vec4 colorIn);

subroutine (alpha)
    vec4 basicTransparency_T_F_O( vec4 colorIn ) { return colorIn; }; 

subroutine (alpha) 
    vec4 basicTransparency_T_F_C( vec4 colorIn ) 
    { 
        if(colorIn.a < basicTransparency_cutoutlevel)
        {
            discard;
        }
        return colorIn;
    };

subroutine (alpha)
    vec4 basicTransparency_T_F_T( vec4 colorIn ) { return colorIn; }; 

subroutine uniform alpha basicTransparency_T_S;


PBR_FRAG_OUT fragmentPBR()
{
    PBR_FRAG_OUT outFrag;  
    outFrag.albedo = basicTransparency_T_S(vec4(1, 1, 1, 1));
    outFrag.metallic = 0.1;
    outFrag.roughness = 0.9;
    outFrag.normal = fs_in.Tnormal;
    return outFrag;
}