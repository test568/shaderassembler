#include <iostream>

#include "comandReader/comandReader.h"
#include "comandReader/comand.h"

#include "errorMessage.h"

#include "parser/parser.h"
#include "shader/shader.h"
#include "compiler/compiler.h"
#include "shaderWriter/shaderWriter.h"

void printErrors(const std::list<Error> *_errors)
{
	for (auto i : *_errors)
	{
		if (i.type == TypeError::CRITICAL)
		{
			printf("Critical: %s\n", i.message.c_str());
		}
		else
		{
			printf("Warning: %s\n", i.message.c_str());
		}
	}
}

int main(int argc, char *argv[])
{
	ErrorsContainer errors;

	Comand *comand = ComandReader::readComand(argc, argv);
	if (comand == NULL)
	{
		std::cout << "Arguments are not correct." << std::endl;
		return 0;
	}

	Parser parser = Parser(&errors);
	parser.parse(comand->path);

	if (errors.hasFatalErrors())
	{
		printErrors(errors.getListErrors());
		return 0;
	}

	Shader shader;
	shader.name = parser.getNameShader();
	shader.properties = parser.getProperties();
	shader.settings = parser.getSettings();
	shader.passes = *parser.getPasses();

	Compiler compiler = Compiler(&errors);
	if (errors.hasFatalErrors())
	{
		printErrors(errors.getListErrors());
		return 0;
	}

	compiler.compileShader(&shader);

	if (errors.hasFatalErrors())
	{
		printErrors(errors.getListErrors());
		return 0;
	}

	ShaderWriter shaderWriter = ShaderWriter(&errors);
	shaderWriter.writeShader(&(comand->compilePath), &shader);

	printErrors(errors.getListErrors());
	return 0;
}