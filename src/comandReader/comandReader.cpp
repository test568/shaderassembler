#include "comandReader/comandReader.h"

#include "cstring"

Comand *ComandReader::readComand(int argc, char *argv[])
{
    if(argc != 5)
    {
        return NULL;
    }
    int posInput = -1;
    int posOutput = -1;
    for(int i = 0; i < argc; i++)
    {
        if(checkIsInput(argv[i]))
        {
            if(posInput == -1)
            {
                posInput = i;
            }
            else
            {
                return NULL;
            }
        }
        else if(checkIsOutput(argv[i]))
        {
            if(posOutput == -1)
            {
                posOutput = i;
            }
            else
            {
                return NULL;
            }
        }
    }
    if((posInput != 1 || posOutput != 3) && (posInput != 3 || posOutput != 1))
    {
        return NULL;
    }
    Comand *comand = new Comand();
    comand->path = getPath(argv[posInput + 1]);
    comand->compilePath = getPath(argv[posOutput + 1]);
    return comand;
}

bool ComandReader::checkIsInput(char *arg)
{
    if(std::strcmp(arg, "-I") == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool ComandReader::checkIsOutput(char *arg)
{
    if(std::strcmp(arg, "-O") == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::string ComandReader::getPath(char *arg)
{
    return std::string(arg);
}