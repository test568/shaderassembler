#include "errorsContainer.h"

ErrorsContainer::ErrorsContainer()
{
    wasCriticalErros = false;
}

void ErrorsContainer::push_back(Error _error)
{
    errors.push_back(_error);
    if(_error.type == TypeError::CRITICAL)
    {
        wasCriticalErros = true;
    }
}

bool ErrorsContainer::hasFatalErrors()
{
    return wasCriticalErros;
}