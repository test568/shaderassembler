#include "shaderWriter/shaderWriter.h"

#include "shader/properties/intProperty.h"
#include "shader/properties/floatProperty.h"
#include "shader/properties/colorProperty.h"
#include "shader/properties/cubeMapProperty.h"
#include "shader/properties/textureProperty.h"
#include "shader/properties/toggleProperty.h"
#include "shader/properties/vectorProperty.h"

ShaderWriter::ShaderWriter(ErrorsContainer *_errors) : errors(_errors)
{
}

void ShaderWriter::writeShader(const std::string *_path, const Shader *_shader)
{
    SDL_RWops *file = SDL_RWFromFile(_path->c_str(), "wb");
    if (file == NULL)
    {
        errors->push_back({TypeError::CRITICAL, "The output file didn`t created."});
    }
    writeString(file, &(_shader->name));
    writeProperties(file, _shader);
    writeSettings(file, _shader);
    writeInt(file, (int)_shader->passes.size());
    for (auto i : _shader->passes)
    {
        writePass(file, i);
    }
    file->close(file);
}

void ShaderWriter::writeProperties(SDL_RWops *_file, const Shader *_shader)
{
    writeInt(_file, _shader->properties.size());
    for (auto i : _shader->properties)
    {
        writeProperty(_file, i);
    }
}

void ShaderWriter::writeSettings(SDL_RWops *_file, const Shader *_shader)
{
    writeInt(_file, _shader->settings->indexNormalMapProperty);
    writeInt(_file, _shader->settings->indexclippingTextureProperty);
    writeInt(_file, _shader->settings->typeClippingChanel);
    writeFloat(_file, _shader->settings->clippingValue);
    writeBool(_file, _shader->settings->inverseClipping);
}

void ShaderWriter::writeProperty(SDL_RWops *_file, const Property *_property)
{
    writeString(_file, &(_property->name));
    int type = (int)_property->type;
    writeInt(_file, type);

    if (_property->type == TypeProperty::Int)
    {
        const IntProperty *prop = reinterpret_cast<IntProperty *>(const_cast<Property *>(_property));
        writeInt(_file, prop->defaultValue);
    }
    else if (_property->type == TypeProperty::Float)
    {
        const FloatProperty *prop = reinterpret_cast<FloatProperty *>(const_cast<Property *>(_property));
        writeFloat(_file, prop->defaultValue);
    }
    else if (_property->type == TypeProperty::Toggle)
    {
        const ToggleProperty *prop = reinterpret_cast<ToggleProperty *>(const_cast<Property *>(_property));
        writeBool(_file, prop->defaultValue);
        writeInt(_file, prop->mask);
    }
    else if (_property->type == TypeProperty::Color)
    {
        const ColorProperty *prop = reinterpret_cast<ColorProperty *>(const_cast<Property *>(_property));
        writeFloat(_file, prop->defaultA);
        writeFloat(_file, prop->defaultB);
        writeFloat(_file, prop->defaultG);
        writeFloat(_file, prop->defaultR);
    }
    else if (_property->type == TypeProperty::Vector)
    {
        const VectorProperty *prop = reinterpret_cast<VectorProperty *>(const_cast<Property *>(_property));
        writeFloat(_file, prop->defaultW);
        writeFloat(_file, prop->defaultX);
        writeFloat(_file, prop->defaultY);
        writeFloat(_file, prop->defaultZ);
    }
    else if (_property->type == TypeProperty::Texture)
    {
        const TextureProperty *prop = reinterpret_cast<TextureProperty *>(const_cast<Property *>(_property));
        writeString(_file, &(prop->defaultPath));
        writeBool(_file, prop->isHDRTexture);
    }
    else if (_property->type == TypeProperty::CubeMap)
    {
        const CubeMapProperty *prop = reinterpret_cast<CubeMapProperty *>(const_cast<Property *>(_property));
        writeString(_file, &(prop->defaultPath));
    }
}

void ShaderWriter::writePass(SDL_RWops *_file, const Pass *_pass)
{
    writeString(_file, &(_pass->name));
    int typePass = _pass->typePass;
    writeInt(_file, typePass);

    if (_pass->additionalData != NULL)
    {
        writeBool(_file, true);
        writeInt(_file, _pass->sizeAdditionalData);
        for (int i = 0; i < _pass->sizeAdditionalData; i++)
        {
            writeChar(_file, _pass->additionalData[i]);
        }
    }
    else
    {
        writeBool(_file, false);
    }

    writeBool(_file, _pass->useDefaultPrograms);
    writeBool(_file, _pass->useNormalMap);
    writeBool(_file, _pass->useClippingMap);

    writeSettingPass(_file, &(_pass->settingPass));

    writeInt(_file, _pass->maskUsingDefinesVertex);
    writeInt(_file, _pass->maskUsingDefinesFragment);

    int countVertex = _pass->vertexPrograms.size();
    writeInt(_file, countVertex);
    // printf("count vertex programs: %i\n", countVertex);
    for (auto i = _pass->vertexPrograms.begin(); i != _pass->vertexPrograms.end(); ++i)
    {
        writeProgram(_file, &(*i));
    }

    int countFragment = _pass->fragmentPrograms.size();
    writeInt(_file, countFragment);
    for (auto i = _pass->fragmentPrograms.begin(); i != _pass->fragmentPrograms.end(); ++i)
    {
        writeProgram(_file, &(*i));
    }
}

void ShaderWriter::writeSettingPass(SDL_RWops *_file, const SettingsPass *_settingPass)
{
    int val = _settingPass->queueRender;
    writeInt(_file, val);
    val = _settingPass->renderType;
    writeInt(_file, val);
    val = _settingPass->cullType;
    writeInt(_file, val);
    writeBool(_file, _settingPass->zTest);
    writeBool(_file, _settingPass->zWrite);
    writeBool(_file, _settingPass->ganerateInstancing);
}

void ShaderWriter::writeProgram(SDL_RWops *_file, const Program *_program)
{
    writeInt(_file, (int)_program->maskDefines);

    writeString(_file, &(_program->program));
    writeInt(_file, _program->propertiesUsage.size());
    for (auto i : _program->propertiesUsage)
    {
        writeInt(_file, i.second);
        writeString(_file, &i.first);
    }

    int count = (int)_program->uniformBlocksUsage.size();
    writeInt(_file, count);
    for (int i = 0; i < count; i++)
    {
        writeString(_file, &(_program->uniformBlocksUsage[i]));
    }
}

void ShaderWriter::writeInt(SDL_RWops *_file, int _val)
{
    _file->write(_file, &_val, sizeof(int), 1);
}

void ShaderWriter::writeChar(SDL_RWops *_file, char _val)
{
    _file->write(_file, &_val, sizeof(char), 1);
}

void ShaderWriter::writeFloat(SDL_RWops *_file, float _val)
{
    _file->write(_file, &_val, sizeof(float), 1);
}

void ShaderWriter::writeString(SDL_RWops *_file, const std::string *_val)
{
    writeInt(_file, _val->size());
    _file->write(_file, _val->c_str(), sizeof(char), _val->size());
}

void ShaderWriter::writeBool(SDL_RWops *_file, bool _val)
{
    _file->write(_file, &_val, sizeof(bool), 1);
}