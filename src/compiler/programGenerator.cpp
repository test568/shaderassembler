#include <set>

#include "compiler/programGenerator.h"
#include "compiler/addtitionalDefines.h"

void ProgramGenerator::generateMainPrograms(Pass *_pass)
{
    _pass->vertexFull = "";
    _pass->fragmentFull = "";
    for (auto i : _pass->libs)
    {
        _pass->vertexFull += i;
        _pass->vertexFull += "\n";

        _pass->fragmentFull += i;
        _pass->fragmentFull += "\n";
    }

    _pass->vertexFull += _pass->vertex;
    _pass->fragmentFull += _pass->fragment;
}

std::string ProgramGenerator::generateVertexProgram(const Pass *_pass, const std::vector<std::string> *_defines)
{
    std::string vertex = std::string(VERSION_PROGRAM);
    vertex += generateCodeDefines(&(_pass->staticDefines));
    vertex += generateCodeDefines(&(AdditionalDefines::vertexDefines));
    std::vector<std::string> additionalDefines = getAdditionalsPassDefines(_pass);
    vertex += generateCodeDefines(&additionalDefines);
    vertex += generateCodeDefines(_defines);
    if (_pass->settingPass.ganerateInstancing)
    {
        vertex += IF_USE_INSTANCING_DDEFINE;
        vertex += MAX_COUNT_INSTANCED_OBJECTS;
    }
    vertex += _pass->vertexFull;
    vertex += "\n";
    return vertex;
}

std::string ProgramGenerator::generateFragmentProgram(const Pass *_pass, const std::vector<std::string> *_defines)
{
    std::string fragment = std::string(VERSION_PROGRAM);
    fragment += generateCodeDefines(&(_pass->staticDefines));
    fragment += generateCodeDefines(&(AdditionalDefines::fragmentDefines));
    std::vector<std::string> additionalDefines = getAdditionalsPassDefines(_pass);
    fragment += generateCodeDefines(&additionalDefines);
    fragment += generateCodeDefines(_defines);
    fragment += _pass->fragmentFull;
    fragment += "\n";
    return fragment;
}

std::vector<std::string> ProgramGenerator::getAdditionalsPassDefines(const Pass *_pass)
{
    std::vector<std::string> resultDefines;
    if (_pass->typePass == TypePass::Surface)
    {
        char useAdditionalLayers = _pass->additionalData[0];
        for (int i = 0; i < MAX_COUNT_ADDITIONAL_LAYERS_IN_G_BUFFER; i++)
        {
            bool useLayer = false;
            useLayer = (useAdditionalLayers & (1 << i)) != 0;
            if (useLayer)
            {
                resultDefines.push_back(std::string(IF_USE_ADDITIONAL_LAYER) + std::to_string(i + 1));
            }
        }
    }

    return resultDefines;
}

std::vector<std::vector<std::string>> ProgramGenerator::generateCombinateDefinesVertex(const Pass *_pass)
{
    std::set<std::string> allDefines;
    for (auto i : _pass->genericDefines)
    {
        allDefines.insert(i);
    }
    for (auto i : _pass->vertexDefines)
    {
        allDefines.insert(i);
    }
    std::vector<std::string> vecDefines = std::vector<std::string>(allDefines.begin(), allDefines.end());

    std::vector<std::vector<std::string>> results = generateCombitations(&vecDefines);
    return results;
}

std::vector<std::vector<std::string>> ProgramGenerator::generateCombinateDefinesFragment(const Pass *_pass)
{
    std::set<std::string> allDefines;
    for (auto i : _pass->genericDefines)
    {
        allDefines.insert(i);
    }
    for (auto i : _pass->fragmentDefines)
    {
        allDefines.insert(i);
    }
    std::vector<std::string> vecDefines = std::vector<std::string>(allDefines.begin(), allDefines.end());

    std::vector<std::vector<std::string>> results = generateCombitations(&vecDefines);
    return results;
}

std::string ProgramGenerator::generateCodeDefines(const std::vector<std::string> *_defines)
{
    std::string defines = "\n";
    for (int i = 0; i < (int)_defines->size(); i++)
    {
        defines += "#define " + _defines->at(i) + "\n";
    }
    defines += "\n";
    return defines;
}

std::vector<std::vector<std::string>> ProgramGenerator::generateCombitations(const std::vector<std::string> *_combinations)
{
    std::vector<std::vector<std::string>> result;
    int count = 1 << _combinations->size();
    for (char i = 0; i < count; i++)
    {
        std::vector<std::string> sub;
        for (int i2 = 0; i2 < 8 && i2 < (int)_combinations->size(); i2++)
        {
            if ((i & (1 << i2)) != 0)
            {
                sub.push_back(_combinations->at(i2));
            }
        }

        result.push_back(sub);
    }
    return result;
}