#include "compiler/additionalProperties.h"

std::vector<std::string> AdditionalProperties::genericProperties = {
    "_Time",

    // Buffers
    "_environmentBuffer",
    "_depthBuffer",
    "_normalBuffer",

    "_viewSize",
};

std::vector<std::string> AdditionalProperties::depthPassProperties = {

};

std::vector<std::string> AdditionalProperties::depthNormalPassProperties = {

};

std::vector<std::string> AdditionalProperties::normalPassProperties = {

};

std::vector<std::string> AdditionalProperties::baseColorPassProperties = {

};

std::vector<std::string> AdditionalProperties::surfacePassProperties = {
    "_modelLight",
};

std::vector<std::string> AdditionalProperties::shadowPassProperties = {

};