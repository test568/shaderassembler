#include "compiler/specialUniformBlocks.h"

std::vector<std::string> SpecialUniformBlocks::specialUniformBlocks = {
    "matricesCamera",
    "matricesObject",
    "skyData",
    "allPointsLightsDataBlock"
};