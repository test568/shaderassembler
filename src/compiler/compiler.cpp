#include <algorithm>

#include "shader/properties/toggleProperty.h"

#include "compiler/compiler.h"

#include "compiler/additionalProperties.h"
#include "compiler/addtitionalDefines.h"
#include "compiler/specialUniformBlocks.h"

Compiler::Compiler(ErrorsContainer *_errors) : errors(_errors)
{
    loaderOpenGlContext.load();
    if (loaderOpenGlContext.allNormal() == false)
    {
        errors->push_back({TypeError::CRITICAL, "The OpenGL context not created."});
    }
}

bool Compiler::compileShader(Shader *_shader)
{
    if (loaderOpenGlContext.allNormal() == false)
    {
        return false;
    }
    compileSettings(_shader);
    for (auto i : _shader->passes)
    {
        addAdditionalDefines(i);
        compilePass(_shader, i);
    }
    return true;
}

void Compiler::compileSettings(Shader *_shader)
{
    _shader->settings->indexNormalMapProperty = -1;
    _shader->settings->indexclippingTextureProperty = -1;
    int index = 0;
    for (auto i = _shader->properties.begin(); i != _shader->properties.end(); ++i)
    {
        if ((*i)->type == TypeProperty::Texture && _shader->settings->normalMap == (*i)->name)
        {
            _shader->settings->indexNormalMapProperty = index;
        }
        else if ((*i)->type == TypeProperty::Texture && _shader->settings->clippingTexture == (*i)->name)
        {
            _shader->settings->indexclippingTextureProperty = index;
        }
        index++;
    }
}

void Compiler::compilePass(Shader *_shader, Pass *_pass)
{
    if (_pass->useDefaultPrograms)
    {
        _pass->maskUsingDefinesVertex = INT32_MAX;
        _pass->maskUsingDefinesFragment = INT32_MAX;
        _pass->vertexPrograms.clear();
        _pass->fragmentPrograms.clear();
        return;
    }

    programGenerator.generateMainPrograms(_pass);

    removeNonUseDefines(_pass);

    std::vector<std::string> removeDefines;
    removeDefines = compileDefines((&_pass->genericDefines), &(_pass->vertexDefines));
    if (removeDefines.size() != 0)
    {
        std::string error = "In pass " + _pass->name + " from vertex shader, were removed defines: ";
        for (auto i : removeDefines)
        {
            error += i + ", ";
        }
        errors->push_back({TypeError::WARNING, error});
    }

    removeDefines = compileDefines((&_pass->genericDefines), &(_pass->fragmentDefines));
    if (removeDefines.size() != 0)
    {
        std::string error = "In pass " + _pass->name + " from fragment shader, were removed defines: ";
        for (auto i : removeDefines)
        {
            error += i + ", ";
        }
        errors->push_back({TypeError::WARNING, error});
    }

    _pass->maskUsingDefinesVertex = _pass->maskUsingDefinesVertex | generateMaskDefines(&_pass->genericDefines, &(_shader->properties));
    _pass->maskUsingDefinesVertex = _pass->maskUsingDefinesVertex | generateMaskDefines(&_pass->vertexDefines, &(_shader->properties));

    _pass->maskUsingDefinesFragment = _pass->maskUsingDefinesFragment | generateMaskDefines(&_pass->genericDefines, &(_shader->properties));
    _pass->maskUsingDefinesFragment = _pass->maskUsingDefinesFragment | generateMaskDefines(&_pass->fragmentDefines, &(_shader->properties));

    std::vector<std::vector<std::string>> combinationsVertex = programGenerator.generateCombinateDefinesVertex(_pass);
    std::vector<std::vector<std::string>> combinationsFragment = programGenerator.generateCombinateDefinesFragment(_pass);

    std::vector<std::string> addtionalProperties = getAdditionalProperties(_pass);

    for (auto i : combinationsVertex)
    {
        Program vertexProgram;
        vertexProgram.maskDefines = generateMaskDefines(&i, &(_shader->properties));
        vertexProgram.program = programGenerator.generateVertexProgram(_pass, &i);

        CheckerProgram checker = CheckerProgram(&(vertexProgram.program), GL_VERTEX_SHADER, errors);
        if (checker.compileWithErrors())
        {
            continue;
        }
        std::list<std::pair<std::string, TypeProperty>> useProperties = checker.getUseProperties(&_shader->properties, &addtionalProperties);
        /// test on use properties
        vertexProgram.propertiesUsage = useProperties;
        vertexProgram.uniformBlocksUsage = checker.getUseUniformBlocks(&SpecialUniformBlocks::specialUniformBlocks);

        _pass->vertexPrograms.push_back(vertexProgram);
    }

    for (auto i : combinationsFragment)
    {
        Program fragmentProgram;
        fragmentProgram.maskDefines = generateMaskDefines(&i, &(_shader->properties));
        fragmentProgram.program = programGenerator.generateFragmentProgram(_pass, &i);

        CheckerProgram checker = CheckerProgram(&(fragmentProgram.program), GL_FRAGMENT_SHADER, errors);
        if (checker.compileWithErrors())
        {
            continue;
        }
        std::list<std::pair<std::string, TypeProperty>> useProperties = checker.getUseProperties(&_shader->properties, &addtionalProperties);
        /// test on use properties
        fragmentProgram.propertiesUsage = useProperties;
        fragmentProgram.uniformBlocksUsage = checker.getUseUniformBlocks(&SpecialUniformBlocks::specialUniformBlocks);

        _pass->fragmentPrograms.push_back(fragmentProgram);
    }
}

int Compiler::getCountExcessDefines(std::vector<std::string> *_genericDefines, std::vector<std::string> *_specialDefines)
{
    return std::max((int)(_genericDefines->size() + _specialDefines->size() - 8), 0);
}

std::vector<std::string> Compiler::compileDefines(std::vector<std::string> *_genericDefines, std::vector<std::string> *_specialDefines)
{
    std::vector<std::string> removeDefines;
    int countExcess = getCountExcessDefines(_genericDefines, _specialDefines);
    if (countExcess != 0)
    {
        int countRemoveFromGeneric = countExcess / 2;
        int cuntRemoveFromSpecial = countExcess - countRemoveFromGeneric;
        for (int i = 0; i < countRemoveFromGeneric; i++)
        {
            removeDefines.push_back(_genericDefines->back());
            _genericDefines->pop_back();
        }
        for (int i = 0; i < cuntRemoveFromSpecial; i++)
        {
            removeDefines.push_back(_specialDefines->back());
            _specialDefines->pop_back();
        }
    }
    return removeDefines;
}

int Compiler::generateMaskDefines(const std::vector<std::string> *_defines, const std::list<Property *> *_properties)
{
    int mask = 0;
    for (auto i : *_defines)
    {
        for (auto i2 : *_properties)
        {
            if (i2->type == TypeProperty::Toggle)
            {
                ToggleProperty *toggle = reinterpret_cast<ToggleProperty *>(i2);
                if (toggle->define == i)
                {
                    mask = mask | toggle->mask;
                }
            }
        }
    }
    return mask;
}

void Compiler::removeNonUseDefines(Pass *_pass)
{
    std::vector<std::string> newDefines;
    for (auto i : _pass->vertexDefines)
    {
        if (_pass->vertexFull.find(i) != std::string::npos)
        {
            newDefines.push_back(i);
        }
    }
    _pass->vertexDefines = newDefines;
    newDefines.clear();

    for (auto i : _pass->fragmentDefines)
    {
        if (_pass->fragmentFull.find(i) != std::string::npos)
        {
            newDefines.push_back(i);
        }
    }
    _pass->fragmentDefines = newDefines;
    newDefines.clear();

    for (auto i : _pass->genericDefines)
    {
        if (_pass->fragmentFull.find(i) != std::string::npos || _pass->vertexFull.find(i) != std::string::npos)
        {
            newDefines.push_back(i);
        }
    }
    _pass->genericDefines = newDefines;
}

std::vector<std::string> Compiler::getAdditionalProperties(Pass *_pass)
{
    std::vector<std::string> result = std::vector<std::string>(AdditionalProperties::genericProperties.begin(), AdditionalProperties::genericProperties.end());

    if (_pass->typePass == TypePass::BaseColor)
    {
        for (auto i : AdditionalProperties::baseColorPassProperties)
        {
            result.push_back(i);
        }
    }
    else if (_pass->typePass == TypePass::Depth)
    {
        for (auto i : AdditionalProperties::depthPassProperties)
        {
            result.push_back(i);
        }
    }
    else if (_pass->typePass == TypePass::Normal)
    {
        for (auto i : AdditionalProperties::normalPassProperties)
        {
            result.push_back(i);
        }
    }
    else if (_pass->typePass == TypePass::DepthNormal)
    {
        for (auto i : AdditionalProperties::depthNormalPassProperties)
        {
            result.push_back(i);
        }
    }
    else if (_pass->typePass == TypePass::Surface)
    {
        for (auto i : AdditionalProperties::surfacePassProperties)
        {
            result.push_back(i);
        }
    }
    else if (_pass->typePass == TypePass::Shadow)
    {
        for (auto i : AdditionalProperties::shadowPassProperties)
        {
            result.push_back(i);
        }
    }

    return result;
}

void pushDefineInVector(std::vector<std::string> *_vector, std::string _str)
{
    if (std::find(_vector->begin(), _vector->end(), _str) == _vector->end())
    {
        _vector->push_back(_str);
    }
}

void Compiler::addAdditionalDefines(Pass *_pass)
{
    for (auto i : AdditionalDefines::genericDefines)
    {
        pushDefineInVector(&_pass->staticDefines, i);
    }

    if (_pass->typePass == TypePass::BaseColor)
    {
        for (auto i : AdditionalDefines::baseColorPassDefines)
        {
            pushDefineInVector(&_pass->staticDefines, i);
        }
    }
    else if (_pass->typePass == TypePass::Depth)
    {
        for (auto i : AdditionalDefines::depthPassDefines)
        {
            pushDefineInVector(&_pass->staticDefines, i);
        }
    }
    else if (_pass->typePass == TypePass::Normal)
    {
        for (auto i : AdditionalDefines::normalPassDefines)
        {
            pushDefineInVector(&_pass->staticDefines, i);
        }
    }
    else if (_pass->typePass == TypePass::DepthNormal)
    {
        for (auto i : AdditionalDefines::depthNormalPassDefines)
        {
            pushDefineInVector(&_pass->staticDefines, i);
        }
    }
    else if (_pass->typePass == TypePass::Surface)
    {
        for (auto i : AdditionalDefines::surfacePassDefines)
        {
            pushDefineInVector(&_pass->staticDefines, i);
        }
    }
    else if (_pass->typePass == TypePass::Shadow)
    {
        for (auto i : AdditionalDefines::shadowPassDefines)
        {
            pushDefineInVector(&_pass->staticDefines, i);
        }
    }

    if (_pass->settingPass.renderType == RenderType::RenderType_Opaque)
    {
        pushDefineInVector(&_pass->staticDefines, "RENDER_TYPE_OPAQUE");
    }
    else if (_pass->settingPass.renderType == RenderType::RenderType_Transparent)
    {
        pushDefineInVector(&_pass->staticDefines, "RENDER_TYPE_TRANSPARENT");
    }
    else if (_pass->settingPass.renderType == RenderType::RenderType_TransparentCutout)
    {
        pushDefineInVector(&_pass->staticDefines, "RENDER_TYPE_TRANSPARENT_CUTOUT");
    }
    else if (_pass->settingPass.renderType == RenderType::RenderType_Terrain)
    {
        pushDefineInVector(&_pass->staticDefines, "RENDER_TYPE_TERRAIN");
    }
    else if (_pass->settingPass.renderType == RenderType::RenderType_TerrainTree)
    {
        pushDefineInVector(&_pass->staticDefines, "RENDER_TYPE_TERRAIN_TREE");
    }
    else if (_pass->settingPass.renderType == RenderType::RenderType_Water)
    {
        pushDefineInVector(&_pass->staticDefines, "RENDER_TYPE_WATER");
    }
}