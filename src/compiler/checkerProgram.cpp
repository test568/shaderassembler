#include <algorithm>
#include <math.h>
#include <set>
#include <iostream>
#include <cstring>

#include "compiler/checkerProgram.h"

CheckerProgram::CheckerProgram(const std::string *_program, GLenum _type, ErrorsContainer *_errors) : program(0), wasErrors(false), errors(_errors)
{
    const char *program_c = _program->c_str();
    program = glCreateShaderProgramv(_type, 1, &program_c);

    GLint link = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &link);
    if (link == GL_FALSE)
    {
        wasErrors = true;

        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
        char *errorLog = new char[maxLength + 1];
        errorLog[maxLength] = 0;
        glGetProgramInfoLog(program, maxLength, &maxLength, errorLog);

        if (maxLength != 0)
        {
            std::string error = std::string(errorLog);

            error += std::string("\n-Pass-\n") + std::string((char *)glGetString(GL_VERSION)) + std::string("\n") + *_program;
            errors->push_back({TypeError::CRITICAL, error});
        }
        else
        {
            errors->push_back({TypeError::CRITICAL, "Compilation shader. None define error."});
        }
        delete[] errorLog;
        glDeleteShader(program);
        return;
    }
}

CheckerProgram::~CheckerProgram()
{
    if (!wasErrors)
    {
        glDeleteShader(program);
    }
}

bool CheckerProgram::compileWithErrors()
{
    return wasErrors;
}

std::vector<std::string> CheckerProgram::getUseUniformBlocks(std::vector<std::string> *_uniformBlocksNames)
{
    std::vector<std::string> useUniformBlocks;
    auto end = _uniformBlocksNames->end();
    for (auto i = _uniformBlocksNames->begin(); i != end; ++i)
    {
        const char *nameUniformBlock = (*i).c_str();
        GLuint location = glGetUniformBlockIndex(program, nameUniformBlock);
        if (location != GL_INVALID_INDEX)
        {
            useUniformBlocks.push_back(nameUniformBlock);
        }
    }
    return useUniformBlocks;
}

std::list<std::pair<std::string, TypeProperty>> CheckerProgram::getUseProperties(std::list<Property *> *_properties, std::vector<std::string> *_nonDefineProperties)
{
    int countUseUniforms = 0;
    glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &countUseUniforms);

    std::vector<std::pair<GLint, GLenum>> locationProperties =
        std::vector<std::pair<GLint, GLenum>>(_properties->size(), std::make_pair(-1, GL_NONE));
    std::vector<std::pair<GLint, GLenum>> locationNoneDefineProperties =
        std::vector<std::pair<GLint, GLenum>>(_nonDefineProperties->size(), std::make_pair(-1, GL_NONE));
    GLenum type = 0;
    GLint size = 0;
    char name[256];
    int countWrite = 0;
    for (int i = 0; i < countUseUniforms; i++)
    {
        type = 0;
        size = 0;
        countWrite = 0;
        glGetActiveUniform(program, i, 256, &countWrite, &size, &type, name);
        name[countWrite] = 0;
        int n = 0;
        for (auto i2 : *_properties)
        {
            if (i2->type != TypeProperty::Toggle && std::strcmp(i2->name.c_str(), name) == 0)
            {
                locationProperties[n] = std::make_pair(i, type);
            }
            n++;
        }
        n = 0;
        for (auto i2 : *_nonDefineProperties)
        {
            if (std::strcmp(i2.c_str(), name) == 0)
            {
                locationNoneDefineProperties[n] = std::make_pair(i, type);
            }
            n++;
        }
    }

    std::set<std::pair<std::string, TypeProperty>> useProperties;
    int n = 0;
    for (auto i : *_properties)
    {
        GLint location = locationProperties[n].first;
        GLenum type = locationProperties[n].second;
        n++;

        bool allNormal = false;

        if (location == -1)
        {
            continue;
        }

        if (i->type == TypeProperty::Float && type == GL_FLOAT)
        {
            allNormal = true;
        }
        else if (i->type == TypeProperty::Int && type == GL_INT)
        {
            allNormal = true;
        }
        else if (i->type == TypeProperty::Color && type == GL_FLOAT_VEC4)
        {
            allNormal = true;
        }
        else if (i->type == TypeProperty::Vector && (type == GL_FLOAT_VEC2 || type == GL_FLOAT_VEC3 || type == GL_FLOAT_VEC4))
        {
            allNormal = true;
        }
        else if (i->type == TypeProperty::Texture && type == GL_SAMPLER_2D)
        {
            allNormal = true;
        }
        else if (i->type == TypeProperty::CubeMap && type == GL_SAMPLER_CUBE)
        {
            allNormal = true;
        }

        if (allNormal)
        {
            useProperties.insert(std::make_pair(i->name, i->type));
        }
    }

    for (int i = 0; i < (int)_nonDefineProperties->size(); i++)
    {
        GLint location = locationNoneDefineProperties[i].first;
        if (location == -1)
        {
            continue;
        }
        useProperties.insert(std::make_pair(_nonDefineProperties->at(i), TypeProperty::Internal));
    }
    std::list<std::pair<std::string, TypeProperty>> listResult = std::list<std::pair<std::string, TypeProperty>>(useProperties.begin(), useProperties.end());
    return listResult;
}