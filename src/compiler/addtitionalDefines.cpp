#include "compiler/addtitionalDefines.h"

std::vector<std::string> AdditionalDefines::genericDefines = {
    "COMPILE_RELEASE",
};

std::vector<std::string> AdditionalDefines::vertexDefines
{
    "VERTEX_SHADER",
};

std::vector<std::string> AdditionalDefines::fragmentDefines
{
    "FRAGMENT_SHADER",
};

std::vector<std::string> AdditionalDefines::baseColorPassDefines = {
    "BASE_COLOR_PASS",
};

std::vector<std::string> AdditionalDefines::surfacePassDefines = {
    "SURFACE_PASS",
};

std::vector<std::string> AdditionalDefines::depthPassDefines = {
    "DEPTH_PASS",
};

std::vector<std::string> AdditionalDefines::normalPassDefines = {
    "NORMAL_PASS",
};

std::vector<std::string> AdditionalDefines::depthNormalPassDefines = {
    "DEPTH_NORMAL_PASS",
};

std::vector<std::string> AdditionalDefines::shadowPassDefines = {
    "SHADOW_PASS",
};
