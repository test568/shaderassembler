#include "compiler/loaderOpenGLContext.h"

LoaderOpenGLContext::LoaderOpenGLContext()
{
}

LoaderOpenGLContext::~LoaderOpenGLContext()
{
    unload();
}

void LoaderOpenGLContext::load()
{
    unload();

    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow("", 0, 0, 10, 10, SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN | SDL_WINDOW_SKIP_TASKBAR);
    if (window == NULL)
    {
        errorInit = true;
        return;
    }

    context = SDL_GL_CreateContext(window);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 6);
}

void LoaderOpenGLContext::unload()
{
    if (errorInit || window == NULL)
    {
        return;
    }
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);

    errorInit = false;
    window = NULL;
}

bool LoaderOpenGLContext::allNormal()
{
    return errorInit;
}