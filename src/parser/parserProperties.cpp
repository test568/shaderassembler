#include <set>
#include <string>

#include "parser/parserProperties.h"

#include "shader/properties/colorProperty.h"
#include "shader/properties/cubeMapProperty.h"
#include "shader/properties/floatProperty.h"
#include "shader/properties/intProperty.h"
#include "shader/properties/textureProperty.h"
#include "shader/properties/toggleProperty.h"
#include "shader/properties/vectorProperty.h"

void stackDump(lua_State *L)
{
    printf("----stack dump----\n");
    int top = lua_gettop(L);
    for (int i = 1; i <= top; i++)
    {
        int type = lua_type(L, i);
        if (LUA_TSTRING == type)
        {
            printf("%s", lua_tostring(L, i));
        }
        else if (LUA_TBOOLEAN == type)
        {
            printf(lua_toboolean(L, i) ? "true" : "false");
        }
        else if (LUA_TNUMBER == type)
        {
            printf("%g", lua_tonumber(L, i));
        }
        else
        {
            printf("%s", lua_typename(L, type));
        }
        printf("\n");
    }
    printf("\n");
}

ParserProperties::ParserProperties(lua_State *L, ErrorsContainer *_errors, int _indexProperties)
    : L(L), errors(_errors), indexProperties(_indexProperties)
{
    wasParse = false;
}

std::list<Property *> ParserProperties::generateListProperties()
{
    if (wasParse)
    {
        return properties;
    }
    wasParse = true;

    lua_rawgeti(L, LUA_REGISTRYINDEX, indexProperties);
    lua_pushnil(L);

    while (lua_next(L, -2))
    {
        if (lua_type(L, -2) != LUA_TSTRING)
        {
            errors->push_back({TypeError::WARNING, "The property name type is not string."});
            lua_pop(L, 1);
            continue;
        }
        std::string nameProperty = lua_tostring(L, -2);
        if (lua_type(L, -1) != LUA_TTABLE)
        {
            errors->push_back({TypeError::WARNING, "Invalid define property: " + nameProperty});
            lua_pop(L, 1);
            continue;
        }
        lua_getfield(L, -1, "type");
        if (lua_type(L, -1) != LUA_TSTRING)
        {
            errors->push_back({TypeError::WARNING, "Invalid define property: " + nameProperty});
            lua_pop(L, 2);
            continue;
        }

        std::string type = lua_tostring(L, -1);
        lua_pop(L, 1);

        if (nameProperty.empty())
        {
            errors->push_back({TypeError::WARNING, "The property name is empty."});
            lua_pop(L, 1);
            continue;
        }

        if (type.empty())
        {
            errors->push_back({TypeError::WARNING, "The property type is empty."});
            lua_pop(L, 1);
            continue;
        }

        if (checkBeingProperty(nameProperty, &properties))
        {
            errors->push_back({TypeError::WARNING, "Property: " + nameProperty + " has already been defined."});
            lua_pop(L, 1);
            continue;
        }
        Property *property = generatePropery(nameProperty, type);
        if (property != NULL)
        {
            properties.push_back(property);
        }
        else
        {
            errors->push_back({TypeError::WARNING, "Property: " + nameProperty + " has no-correct define."});
        }
        lua_pop(L, 1);
    }

    lua_remove(L, -1);

    std::set<std::string> defines;
    for (auto i : properties)
    {
        if (i->type == TypeProperty::Toggle)
        {
            defines.insert(reinterpret_cast<ToggleProperty *>(i)->define);
        }
    }
    for (auto i : properties)
    {
        if (i->type == TypeProperty::Toggle)
        {
            ToggleProperty *toggle = reinterpret_cast<ToggleProperty *>(i);

            int pos = 0;
            for (auto i2 : defines)
            {
                if (toggle->define == i2)
                {
                    break;
                }
                pos++;
            }

            toggle->mask = (int)1 << pos;
        }
    }
    return properties;
}

bool ParserProperties::checkBeingProperty(std::string name, std::list<Property *> *properties)
{
    for (auto i : *properties)
    {
        if (i->name == name)
        {
            return true;
        }
    }
    return false;
}

Property *ParserProperties::generatePropery(std::string name, std::string type)
{
    Property *p = NULL;
    if (type == TYPE_TEXTURE_PROPERTY)
    {
        p = generateTexturePropery(false);
    }
    else if (type == TYPE_HDR_TEXTURE_PROPERTY)
    {
        p = generateTexturePropery(true);
    }
    else if (type == TYPE_CUBE_MAP_PROPERTY)
    {
        p = generateCubeMapPropery();
    }
    else if (type == TYPE_FLOAT_PROPERTY)
    {
        p = generateFloatPropery();
    }
    else if (type == TYPE_INT_PROPERTY)
    {
        p = generateIntPropery();
    }
    else if (type == TYPE_TOGGLE_PROPERTY)
    {
        p = generateTogglePropery();
    }
    else if (type == TYPE_VECTOR_PROPERTY)
    {
        p = generateVectorPropery();
    }
    else if (type == TYPE_COLOR_PROPERTY)
    {
        p = generateColorPropery();
    }

    if (p != NULL)
    {
        p->name = name;
    }

    return p;
}

Property *ParserProperties::generateIntPropery()
{
    IntProperty *p = new IntProperty();
    lua_getfield(L, -1, "default");
    p->defaultValue = 0;
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        p->defaultValue = lua_tointeger(L, -1);
    }
    lua_remove(L, -1);
    p->type = TypeProperty::Int;
    return p;
}

Property *ParserProperties::generateFloatPropery()
{
    FloatProperty *p = new FloatProperty();
    lua_getfield(L, -1, "default");
    p->defaultValue = 0;
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        p->defaultValue = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    p->type = TypeProperty::Float;
    return p;
}

Property *ParserProperties::generateTogglePropery()
{
    ToggleProperty *p = new ToggleProperty();
    lua_getfield(L, -1, "default");
    p->defaultValue = false;
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        p->defaultValue = lua_toboolean(L, -1);
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "define");
    p->define = "TOGGLE";
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        p->define = lua_tostring(L, -1);
    }
    lua_remove(L, -1);
    p->mask = 0;
    p->type = TypeProperty::Toggle;
    return p;
}

Property *ParserProperties::generateVectorPropery()
{
    VectorProperty *p = new VectorProperty();
    lua_getfield(L, -1, "default");
    p->defaultX = 0;
    p->defaultY = 0;
    p->defaultZ = 0;
    p->defaultW = 0;
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        lua_getfield(L, -1, "x");
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            p->defaultX = lua_tonumber(L, -1);
        }
        lua_remove(L, -1);
        lua_getfield(L, -1, "y");
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            p->defaultY = lua_tonumber(L, -1);
        }
        lua_remove(L, -1);
        lua_getfield(L, -1, "z");
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            p->defaultZ = lua_tonumber(L, -1);
        }
        lua_remove(L, -1);
        lua_getfield(L, -1, "w");
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            p->defaultW = lua_tonumber(L, -1);
        }
        lua_remove(L, -1);
    }
    lua_remove(L, -1);
    p->type = TypeProperty::Vector;
    return p;
}

Property *ParserProperties::generateColorPropery()
{
    ColorProperty *p = new ColorProperty();
    lua_getfield(L, -1, "default");
    p->defaultR = 0;
    p->defaultG = 0;
    p->defaultB = 0;
    p->defaultA = 0;
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        lua_getfield(L, -1, "r");
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            p->defaultR = lua_tointeger(L, -1) / 255.0f;
        }
        lua_remove(L, -1);
        lua_getfield(L, -1, "g");
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            p->defaultG = lua_tointeger(L, -1) / 255.0f;
        }
        lua_remove(L, -1);
        lua_getfield(L, -1, "b");
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            p->defaultB = lua_tointeger(L, -1) / 255.0f;
        }
        lua_remove(L, -1);
        lua_getfield(L, -1, "a");
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            p->defaultA = lua_tointeger(L, -1) / 255.0f;
        }
        lua_remove(L, -1);
    }
    lua_remove(L, -1);
    p->type = TypeProperty::Color;
    return p;
}

Property *ParserProperties::generateTexturePropery(bool _isHDRTypeTexture)
{
    TextureProperty *p = new TextureProperty();
    lua_getfield(L, -1, "default");
    p->defaultPath = "white.bmp";
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        p->defaultPath = lua_tostring(L, -1);
    }
    lua_remove(L, -1);
    p->isHDRTexture = _isHDRTypeTexture;
    p->type = TypeProperty::Texture;
    return p;
}

Property *ParserProperties::generateCubeMapPropery()
{
    CubeMapProperty *p = new CubeMapProperty();
    lua_getfield(L, -1, "default");
    p->defaultPath = "white.bmp";

    if (lua_type(L, -1) == LUA_TTABLE)
    {
        lua_getfield(L, -1, "default");
        if (lua_type(L, -1) == LUA_TSTRING)
        {
            p->defaultPath = lua_tostring(L, -1);
        }
        lua_remove(L, -1);
    }
    lua_remove(L, -1);
    p->type = TypeProperty::CubeMap;
    return p;
}