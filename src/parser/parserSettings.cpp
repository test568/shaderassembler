#include "parser/parserSettings.h"

void ParserSettings::addMacroses(lua_State *L)
{
    lua_pushinteger(L, Type_Clipping_Chanel_R);
    lua_setglobal(L, NAME_CHANEL_R);
    lua_pushinteger(L, Type_Clipping_Chanel_G);
    lua_setglobal(L, NAME_CHANEL_G);
    lua_pushinteger(L, Type_Clipping_Chanel_B);
    lua_setglobal(L, NAME_CHANEL_B);
    lua_pushinteger(L, Type_Clipping_Chanel_A);
    lua_setglobal(L, NAME_CHANEL_A);
}

ParserSettings::ParserSettings(lua_State *L, ErrorsContainer *_errors, int _posSettings)
    : L(L), errors(_errors), posSettings(_posSettings), settings(NULL)
{
    settings = new Settings();
    settings->clippingTexture = std::string();
    settings->normalMap = std::string();
    settings->clippingValue = 0.5f;
    settings->inverseClipping = false;
    settings->typeClippingChanel = Type_Clipping_Chanel_A;
}

void ParserSettings::generateSettings()
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, posSettings);
    parseNormalMap();
    parseClippingSettings();
    lua_remove(L, -1);
}

Settings *ParserSettings::getSettings()
{
    return settings;
}

void ParserSettings::parseNormalMap()
{
    lua_getfield(L, -1, "NormalMap");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        settings->normalMap = std::string(lua_tostring(L, -1));
    }
    else
    {
        settings->normalMap = std::string();
    }
    lua_remove(L, -1);
}

void ParserSettings::parseClippingSettings()
{
    lua_getfield(L, -1, "ClippingMap");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        settings->clippingTexture = std::string(lua_tostring(L, -1));
    }
    else
    {
        settings->clippingTexture = std::string();
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "ClippingChanel");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        settings->typeClippingChanel = (TypeClippingChanel)lua_tointeger(L, -1);
    }
    else
    {
        settings->typeClippingChanel = Type_Clipping_Chanel_A;
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "ClippingValue");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        settings->clippingValue = lua_tonumber(L, -1);
        settings->clippingValue = std::min(1.0f, std::max(0.0f, settings->clippingValue));
    }
    else
    {
        settings->clippingValue = 0.5f;
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "InverseClipping");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        settings->inverseClipping = lua_toboolean(L, -1);
    }
    else
    {
        settings->inverseClipping = false;
    }
    lua_remove(L, -1);
}