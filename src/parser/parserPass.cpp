#include <SDL2/SDL.h>
#include <set>
#include <algorithm>
#include <array>

#include "parser/parserPass.h"
#include "parser/parserSettingsPass.h"

ParserPass::ParserPass(lua_State *L, ErrorsContainer *_errors)
    : L(L), errors(_errors)
{
}

void ParserPass::generatePass(SettingsPass _globalSettings)
{
    parseWithErros = true;

    ParserSettingsPass parserSetting = ParserSettingsPass(L, errors);
    SettingsPass localSetting = parserSetting.updateSettingsPass(_globalSettings);
    pass = new Pass();
    pass->settingPass = localSetting;
    parseNamePass();
    parseTypePass();
    parsePriorityPass();

    if (pass->typePass == TypePass::Surface)
    {
        parseAdditionalDataForSurface();
    }
    else
    {
        pass->additionalData = NULL;
        pass->sizeAdditionalData = 0;
    }

    parseIsUseDefaultPrograms();

    if (pass->useDefaultPrograms == false)
    {
        parseListLibgs();
        parseMainShaders();
        parseDefines();
    }
    else
    {
        pass->libs.clear();
        pass->vertex = std::string();
        pass->fragment = std::string();

        pass->staticDefines.clear();
        pass->genericDefines.clear();
        pass->vertexDefines.clear();
        pass->fragmentDefines.clear();
    }
    parseWithErros = false;
}

Pass *ParserPass::getPass()
{
    if (parseWithErros)
    {
        return NULL;
    }
    else
    {
        return pass;
    }
}

void ParserPass::parseNamePass()
{
    pass->name = "Pass";
    lua_getfield(L, -1, "NamePass");
    if (lua_type(L, -1) != LUA_TSTRING)
    {
        errors->push_back({TypeError::WARNING, "The Pass hasn't name."});
        lua_pop(L, 1);
        return;
    }
    pass->name = lua_tostring(L, -1);
    lua_pop(L, 1);

    if (pass->name.empty())
    {
        errors->push_back({TypeError::WARNING, "The Pass has empty name."});
    }
}

void ParserPass::parseTypePass()
{
    pass->typePass = TypePass::BaseColor;
    lua_getfield(L, -1, "TypePass");
    if (lua_type(L, -1) != LUA_TSTRING)
    {
        errors->push_back({TypeError::WARNING, "The Pass hasn't type."});
        lua_pop(L, 1);
        return;
    }
    std::string typePass = lua_tostring(L, -1);
    lua_pop(L, 1);

    if (typePass == "BaseColor")
    {
        pass->typePass = TypePass::BaseColor;
    }
    else if (typePass == "Depth")
    {
        pass->typePass = TypePass::Depth;
    }
    else if (typePass == "DepthNormal")
    {
        pass->typePass = TypePass::DepthNormal;
    }
    else if (typePass == "Surface")
    {
        pass->typePass = TypePass::Surface;
    }
    else if (typePass == "Normal")
    {
        pass->typePass = TypePass::Normal;
    }
    else if (typePass == "Shadow")
    {
        pass->typePass = TypePass::Shadow;
    }
}

void ParserPass::parsePriorityPass()
{
    pass->priority = 1;
    lua_getfield(L, -1, "Priority");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        pass->priority = lua_tointeger(L, -1);
    }
    lua_remove(L, -1);
}

void ParserPass::parseAdditionalDataForSurface()
{
    std::string modelLight = "Standard";
    std::array<bool, MAX_COUNT_ADDITIONAL_LAYERS_IN_G_BUFFER> useAdditionalLayers;
    for (int i = 0; i < MAX_COUNT_ADDITIONAL_LAYERS_IN_G_BUFFER; i++)
    {
        useAdditionalLayers[i] = false;

        lua_getfield(L, -1, (std::string("UseAdditionalLayer_") + std::to_string(i + 1)).c_str());
        if (lua_type(L, -1) == LUA_TBOOLEAN)
        {
            useAdditionalLayers[i] = lua_toboolean(L, -1);
        }
        lua_remove(L, -1);
    }

    lua_getfield(L, -1, "ModelLight");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        modelLight = lua_tostring(L, -1);
    }
    lua_remove(L, -1);

    pass->sizeAdditionalData = modelLight.size() + 1;
    pass->additionalData = new char[pass->sizeAdditionalData];
    char useLayers = 0;
    for (int i = 0; i < MAX_COUNT_ADDITIONAL_LAYERS_IN_G_BUFFER; i++)
    {
        if (useAdditionalLayers[i])
        {
            useLayers = useLayers | (1 << i);
        }
    }
    pass->additionalData[0] = useLayers;

    int countModelLight = (int)modelLight.size();
    for (int i = 0; i < countModelLight; i++)
    {
        pass->additionalData[i + 1] = modelLight[i];
    }
}

void ParserPass::parseIsUseDefaultPrograms()
{
    lua_getfield(L, -1, "UseDefaultPrograms");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        pass->useDefaultPrograms = lua_toboolean(L, -1);
    }
    else
    {
        pass->useDefaultPrograms = false;
    }
    lua_remove(L, -1);

    if (pass->useDefaultPrograms == false)
    {
        pass->useDefaultPrograms = false;
        pass->useNormalMap = false;
        return;
    }

    lua_getfield(L, -1, "UseNormalMap");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        pass->useNormalMap = lua_toboolean(L, -1);
    }
    else
    {
        pass->useNormalMap = false;
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "UseClippingMap");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        pass->useClippingMap = lua_toboolean(L, -1);
    }
    else
    {
        pass->useClippingMap = false;
    }
    lua_remove(L, -1);
}

void ParserPass::parseListLibgs()
{
    pass->libs.clear();
    std::vector<std::string> paths;
    parseGroupStrings("Includes", &paths);

    for (auto i : paths)
    {
        pass->libs.push_back(readAllFile(&i));
    }
}

void ParserPass::parseMainShaders()
{
    lua_getfield(L, -1, "Vertex");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        std::string pathOnVertex = lua_tostring(L, -1);
        pass->vertex = readAllFile(&pathOnVertex);
    }

    lua_getfield(L, -2, "Fragment");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        std::string pathOnFragment = lua_tostring(L, -1);
        pass->fragment = readAllFile(&pathOnFragment);
    }
    lua_pop(L, 2);
}

void ParserPass::parseDefines()
{
    pass->staticDefines.clear();
    parseGroupStrings("StaticDefines", &pass->staticDefines);
    pass->genericDefines.clear();
    parseGroupStrings("GeneralDefines", &pass->genericDefines);
    pass->vertexDefines.clear();
    parseGroupStrings("VertexDefines", &pass->vertexDefines);
    pass->fragmentDefines.clear();
    parseGroupStrings("FragmentDefines", &pass->fragmentDefines);
}

void ParserPass::parseGroupStrings(const char *_nameGroup, std::vector<std::string> *_dest)
{
    lua_getfield(L, -1, _nameGroup);
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_pop(L, 1);
        return;
    }
    lua_pushnil(L);

    std::set<std::string> combs;
    while (lua_next(L, -2))
    {
        if (lua_type(L, -1) == LUA_TSTRING)
        {
            const char *_str = lua_tostring(L, -1);
            if (std::find(_dest->begin(), _dest->end(), std::string(_str)) == _dest->end())
            {
                _dest->push_back(std::string(_str));
            }
        }
        lua_pop(L, 1);
    }
    lua_pop(L, 1);
}

std::string ParserPass::readAllFile(const std::string *_path)
{
    SDL_RWops *file = SDL_RWFromFile(_path->c_str(), "r");
    if (file == NULL)
    {
        errors->push_back({TypeError::CRITICAL, "File: " + *_path + ", didn`t open."});
        return std::string("");
    }
    file->seek(file, 0, RW_SEEK_SET);

    int size = file->size(file);
    char *mem = new char[size + 1];
    mem[size] = 0;
    int pos = 0;
    while (size)
    {
        int count = file->read(file, mem + pos, sizeof(char), size);
        pos += count;
        size -= count;
    }
    std::string data = std::string(mem);
    delete mem;
    file->close(file);
    return data;
}