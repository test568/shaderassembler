#include "parser/parserSubShader.h"

#include <algorithm>

ParserSubShader::ParserSubShader(lua_State *L, ErrorsContainer *_errors, int _indexSubShader)
    : L(L), errors(_errors), indexSubShader(_indexSubShader)
{
}

void ParserSubShader::generateSubShader()
{
    ParserSettingsPass pass = ParserSettingsPass(L, errors);

    lua_rawgeti(L, LUA_REGISTRYINDEX, indexSubShader);
    SettingsPass globalSettings = pass.generateSettingsPass();

    ParserPass parserPass = ParserPass(L, errors);

    lua_pushnil(L);
    while (lua_next(L, -2))
    {
        if (lua_type(L, -1) != LUA_TTABLE)
        {
            lua_pop(L, 1);
            continue;
        }

        lua_getfield(L, -1, "TypePass");
        int type = lua_type(L, -1);
        lua_pop(L, 1);

        if (type != LUA_TSTRING)
        {
            continue;
        }

        parserPass.generatePass(globalSettings);

        Pass *p = parserPass.getPass();
        if (p == NULL)
        {
            lua_pop(L, 1);
            continue;
        }
        bool isCopingDefaultPass = false;
        if (p->useDefaultPrograms)
        {

            for (auto i = passes.begin(); i != passes.end(); ++i)
            {
                if (p->typePass == (*i)->typePass && p->useDefaultPrograms == (*i)->useDefaultPrograms)
                {
                    isCopingDefaultPass = true;
                    break;
                }
            }
        }
        lua_pop(L, 1);

        if (!isCopingDefaultPass)
        {
            passes.push_back(p);
        }
    }

    lua_remove(L, -1);

    std::sort(passes.begin(), passes.end(), [](const Pass *_passFirst, const Pass *_passSecond)
              { return _passFirst->priority > _passSecond->priority; });
}