#include "parser/parserSettingsPass.h"

ParserSettingsPass::ParserSettingsPass(lua_State *L, ErrorsContainer *_errors) : L(L), errors(_errors)
{
}

SettingsPass ParserSettingsPass::generateSettingsPass()
{
    SettingsPass settings;
    settings.zTest = true;
    settings.zWrite = true;
    settings.queueRender = QueueRender::QueueRender_Geometry;
    settings.renderType = RenderType::RenderType_Opaque;
    settings.cullType = CullType::CullType_Back;
    settings.ganerateInstancing = false;
    return updateSettingsPass(settings);
}

SettingsPass ParserSettingsPass::updateSettingsPass(SettingsPass settings)
{
    lua_getfield(L, -1, "ZWrite");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        settings.zWrite = lua_toboolean(L, -1);
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "ZTest");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        settings.zTest = lua_toboolean(L, -1);
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "Queue");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        std::string queueN = lua_tostring(L, -1);
        if (queueN == "AlphaTest")
        {
            settings.queueRender = QueueRender::QueueRender_AlphaTest;
        }
        else if (queueN == "Transparent")
        {
            settings.queueRender = QueueRender::QueueRender_Transparent;
        }
        else if (queueN == "Overlay")
        {
            settings.queueRender = QueueRender::QueueRender_Overlay;
        }
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "RenderType");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        std::string queueN = lua_tostring(L, -1);
        if (queueN == "TransparentCutout")
        {
            settings.renderType = RenderType::RenderType_TransparentCutout;
        }
        else if (queueN == "Transparent")
        {
            settings.renderType = RenderType::RenderType_Transparent;
        }
        else if (queueN == "Terrain")
        {
            settings.renderType = RenderType::RenderType_Terrain;
        }
        else if (queueN == "TerrainTree")
        {
            settings.renderType = RenderType::RenderType_TerrainTree;
        }
        else if (queueN == "Water")
        {
            settings.renderType = RenderType::RenderType_Water;
        }
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "Cull");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        std::string queueN = lua_tostring(L, -1);
        if (queueN == "Front")
        {
            settings.cullType = CullType::CullType_Front;
        }
        else if (queueN == "Back")
        {
            settings.cullType = CullType::CullType_Back;
        }
        else if (queueN == "Off")
        {
            settings.cullType = CullType::CullType_Off;
        }
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "GenerateInstancing");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        settings.ganerateInstancing = lua_toboolean(L, -1);
    }
    lua_remove(L, -1);
    return settings;
}