#include "parser/parser.h"

Parser::Parser(ErrorsContainer *_errors) : errors(_errors)
{
    L = NULL;
}

void Parser::parse(std::string path)
{
    L = luaL_newstate();
    ParserSettings::addMacroses(L);

    int error = luaL_dofile(L, path.c_str());

    if (error)
    {
        const char *cstr = lua_tostring(L, -1);
        errors->push_back({TypeError::CRITICAL, std::string(cstr)});
        return;
    }

    lua_getglobal(L, "Shader");
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        errors->push_back({TypeError::CRITICAL, std::string("Block 'Shader' not found.")});
        return;
    }

    lua_getfield(L, -1, "Name");
    if (lua_type(L, -1) != LUA_TSTRING)
    {
        errors->push_back({TypeError::WARNING, std::string("Name shader not found.")});
    }
    else
    {
        nameShader = lua_tostring(L, -1);
        if (nameShader.empty())
        {
            errors->push_back({TypeError::WARNING, std::string("Name shader is empty.")});
        }
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "Properties");
    int posProperties = -1;
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        errors->push_back({TypeError::CRITICAL, std::string("Properties shader not found.")});
        lua_remove(L, -1);
    }
    else
    {
        posProperties = luaL_ref(L, LUA_REGISTRYINDEX);
    }

    lua_getfield(L, -1, "Settings");
    int posSettings = -1;
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_remove(L, -1);
    }
    else
    {
        posSettings = luaL_ref(L, LUA_REGISTRYINDEX);
    }

    lua_getfield(L, -1, "SubShader");
    int posSubShader = -1;
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        errors->push_back({TypeError::CRITICAL, std::string("SubShader not found.")});
        lua_remove(L, -1);
    }
    else
    {
        posSubShader = luaL_ref(L, LUA_REGISTRYINDEX);
    }

    if (errors->hasFatalErrors())
    {
        return;
    }

    lua_settop(L, 0);
    parserProperties = new ParserProperties(L, errors, posProperties);
    parserProperties->generateListProperties();
    if (errors->hasFatalErrors())
    {
        return;
    }

    parserSettings = new ParserSettings(L, errors, posSettings);
    if (posSettings != -1)
    {
        parserSettings->generateSettings();
    }
    if (errors->hasFatalErrors())
    {
        return;
    }

    parserSubShader = new ParserSubShader(L, errors, posSubShader);
    parserSubShader->generateSubShader();
    if (errors->hasFatalErrors())
    {
        return;
    }
}

std::string Parser::getNameShader()
{
    return nameShader;
}

std::list<Property *> Parser::getProperties()
{
    return parserProperties->generateListProperties();
}

const std::vector<Pass *> *Parser::getPasses()
{
    return parserSubShader->getPasses();
}

Settings *Parser::getSettings()
{
    if(parserSettings == NULL)
    {
        return NULL;
    }
    else
    {
        return parserSettings->getSettings();
    }
}