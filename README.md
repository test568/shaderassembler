
# OpenGL Shader Assembler

This project was created for GraphicEngine_2 (https://gitlab.com/Weaponer/graphicengine_2), to generate different version of shader programs for objects in the scene.



## Dependenscies

- OpenGL
- SDL2
- Lua5.2
## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:Weaponer/shaderassembler.git
```

Go to the project directory

```bash
  cd shaderassembler
```

Make program

```bash
  make
```

Example build shader:

-I - input file

-O - output file

```bash
./cshader -I ./standard.shader -O ./standard.cshader
```
## Usage/Examples

- SkyBox shader

```lua
Shader = {
    Name = "CubeSky",
    Properties = {
        _ReflectionTexture = {
            type = "Texture",
        },
    },
    SubShader = {
        ReflectionPass = {
            NamePass = "ReflectionPass",
            TypePass = "BasicColor",
            Priority = 0,

            Cull = "Off",

            Includes = {
                "shaders/newShaders/libs/skyData.glsl",
                "shaders/newShaders/libs/sky.glsl"
            },

            Vertex = "shaders/newShaders/skyShaders/cubeSky/vert.vert",
            Fragment = "shaders/newShaders/skyShaders/cubeSky/frag.frag",
        },
    },
}
```

- Standard shader without textures

```lua
Shader = {
    Name = "Standard",
    Properties = {
        _Color = {
            type = "Color",
            default = { r = 255, g = 255, b = 255, a = 255},
        },
        _Metallic = {
            type = "Float",
            default = 0.01,
        }, 
        _Roughness = {
            type = "Float",
            default = 0.95,
        },
        _AO = {
            type = "Float",
            default = 1.0,
        }, 
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "Opaque",
        GenerateInstancing = true,

        --[[PassOne = {
            NamePass = "BasicPass",
            TypePass = "BasicColor",

            
            Includes = {
                "shaders/newShaders/libs/additionalDefines.glsl",
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/light.glsl",
            },
            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
        DepthNormalPass = {
            NamePass = "DepthNormalPass",
            TypePass = "DepthNormal",
            
            UseDefaultPrograms = true,
            UseNormalMap = false,
            UseClippingMap = false,
        },]]--

        SurfacePass = {
            NamePass = "SurfacePass",
            TypePass = "Surface",

            ModelLight = "Standard",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/surface.glsl",
            },

            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        },
        ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",

            UseDefaultPrograms = true,
            UseNormalMap = false,
            UseClippingMap = false,
        },
    }
}
```

- Standard shader with textures

```lua
Shader = {
    Name = "StandardTextures",
    Properties = {
        _MainTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _NormalTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _MetallicTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _RoughnessTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _AOTex = {
            type = "Texture",
            default = "textures/black.bmp",
        },
        _ScaleOffset = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _Color = {
            type = "Color",
            default = { r = 255, g = 255, b = 255, a = 255},
        },
        _Metallic = {
            type = "Float",
            default = 0.01,
        }, 
        _Roughness = {
            type = "Float",
            default = 0.9,
        }, 
        _AO = {
            type = "Float",
            default = 1.0,
        }, 
    },
    Settings = {
        NormalMap = "_NormalTex",
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "Opaque",
        GenerateInstancing = true,

        PassOne = {
            NamePass = "BasicPass",
            TypePass = "BasicColor",

            StaticDefines = {
                "USE_TEXTURES"
            },

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/light.glsl",
                "shaders/newShaders/libs/modelLightsFormuls/pbr.glsl",
            },
            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
        DepthNormalPass = {
            NamePass = "DepthNormalPass",
            TypePass = "DepthNormal",
            
            UseDefaultPrograms = true,
            UseNormalMap = true,
            UseClippingMap = false,
        },
        ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",

            UseDefaultPrograms = true,
            UseNormalMap = false,
            UseClippingMap = false,
        },
    }
}
```
